﻿using System.Collections.Generic;

using match3.Model;

namespace match3
{
	internal sealed class Statistics
	{
		// key: value -- element type: count
		private readonly Dictionary<int, int> destroyed = new Dictionary<int, int>();
        private readonly Dictionary<int, int> generated = new Dictionary<int, int>();

		public Statistics(Level level)
		{
		    level.onDestroyElementEvent += OnDestroyElement;
		    level.onGenerateElementEvent += OnGenerateElement;
		}

	    public int GetDestroyed(int type)
	    {
	        return destroyed.ContainsKey(type) ? destroyed[type] : 0;
	    }

	    private void OnGenerateElement(Element e)
	    {
            var type = e.Type;

            if (!generated.ContainsKey(type))
                generated[type] = 0;

            generated[type]++;
        }

	    private void OnDestroyElement(Element e)
	    {
	        var type = e.Type;

	        if (!destroyed.ContainsKey(type))
	            destroyed[type] = 0;

	        destroyed[type]++;
	    }
	}
}

