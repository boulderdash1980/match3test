﻿using System;
using System.Collections.Generic;

using match3.Utils;

namespace match3
{
    internal static class Extensions
    {
        public static void AddRange<T>(this HashSet<T> set, IEnumerable<T> range)
        {
            foreach (var e in range)
                set.Add(e);
        }

        public static void AddRange<K, V>(this Dictionary<K, V> dictionary, Dictionary<K, V> range)
        {
            foreach (var e in range)
                dictionary[e.Key] = e.Value;
        }

        public static void RemoveAll<K, V>(this Dictionary<K, V> dict, Predicate<KeyValuePair<K, V>> match)
        {
            var keys = new List<K>(dict.Keys);

            foreach (var key in keys)
            {
                if (match(new KeyValuePair<K, V>(key, dict[key])))
                    dict.Remove(key);
            }
        }

        public static void Shuffle<T>(this IList<T> list)
        {
            for (var i = 0; i < list.Count - 1; i++)
            {
                var j = RandomHelper.Next(i, list.Count);
                var tmp = list[i];

                list[i] = list[j];
                list[j] = tmp;
            }
        }
    }
}