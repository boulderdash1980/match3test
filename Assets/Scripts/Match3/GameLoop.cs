//#define MULTITHREAD
//#define SEND_EVENTS_STEP_BY_STEP

using System.Collections.Generic;
using System.Linq;

using match3.GameData;
using match3.Events.Input;
using match3.Events;
using match3.Exceptions;

using match3.Model.GameConditions;
using match3.Info;
using match3.Model;
using match3.Utils;
using match3.Geom;

#if MULTITHREAD
using System;
using System.Threading;
#endif

namespace match3
{
    public delegate void EventHandler(AbstractEvent e);

    public sealed class GameLoop : IInputHandler
    {
#if MULTITHREAD
        private Thread mainThread;
        private readonly Queue<InputEvent> input = new Queue<InputEvent>();
        private readonly Queue<Exception> exceptions = new Queue<Exception>();
		private readonly AutoResetEvent inputEvent = new AutoResetEvent(false);
#endif
        private readonly Queue<AbstractEvent> events = new Queue<AbstractEvent>();

        private readonly List<MatchInfo> possibleMatches = new List<MatchInfo>();

        private Level level;

		private GameCondition condition;

        private Statistics statistics;

        public event EventHandler onSendEvent = delegate { };

		public void Start(LevelData data, IEnumerable<MatchData> matches, IEnumerable<ElementData> elements)
		{
			level = new Level(data, matches, elements);
            statistics = new Statistics(level);

            condition = new GameCondition(data.GameCondition, level);
            
#if MULTITHREAD
			mainThread = new Thread(MainThread);
			mainThread.Start();
#else
			Start();
#endif
		}

        private void Start()
        {
            AddEvent(level.GenerateField());
            DropAndGenerate();
            FindPossibleMatches();
        }

#if MULTITHREAD
        private void MainThread()
        {
            try
            {
                Start();
            }
            catch (Exception e)
            {
                lock (exceptions)
                {
                    exceptions.Enqueue(e);
                }
            }

            while (true)
            {
                try
                {
					InputEvent e = null;

					lock (input)
                    {
                        if (input.Any())
                        {
                            e = input.Dequeue();
                        }
                    }

					if(e != null)
                    {
						e.Dispatch();
                    }
					else
					{
						inputEvent.WaitOne();
						inputEvent.Reset();
					}
                }
                catch (Exception e)
                {
                    lock (exceptions)
                    {
                        exceptions.Enqueue(e);
                    }
                }
            }
        }
#endif

        private bool DestroyMatches(Match[] matches)
        {
            if (Collections.IsNullOrEmpty(matches))
                return false;

            AddEvent(level.DestroyMatches(matches));
            AddEvent(level.ApplyCombinations(matches));

            return true;
        }

        private void DropAndGenerate()
        {
            do
            {
                AddEvent(level.DropAndGenerate());
            }
            while (DestroyMatches(level.FindAllMatches()));
        }

        private void EndTurn()
        {
            //clearing possible matches for avoid situation when destroyed elements used in hint
            lock(possibleMatches)
                possibleMatches.Clear();

            DropAndGenerate();

            AddEvent(level.OnDropComplete());
            DestroyMatches(level.FindAllMatches());

            DropAndGenerate();

            AddEvent(level.OnEndTurn());
            AddEvent(condition.Check());

            //find matches is game continue
            if(!condition.IsGameComplete())
                FindPossibleMatches();
        }

        private bool FindMatches()
        {
            lock (possibleMatches)
            {
                possibleMatches.Clear();

                var blast = level.GetBlastElements();
                var matches = level.FindAllPossibleMatches();

                possibleMatches.AddRange(blast.Select(e => new MatchInfo(-1, e.Id)));
                possibleMatches.AddRange(matches.Select(e => new MatchInfo(e.Priority, e.Elements.Select(i => i.Id))));

                return possibleMatches.Any();
            }
        }

        private void FindPossibleMatches()
        {
            if (FindMatches())
                return;

            var shuffle = level.ShuffleField();

            AddEvent(FindMatches() ? shuffle : new GameEndEvent(GameEndStatus.NotEnoughtMatches));
        }

        private void AddEvent(AbstractEvent e)
        {
            if (e == null)
                return;

            lock (events)
                events.Enqueue(e);

#if SEND_EVENTS_STEP_BY_STEP
            SendEvents();
#endif
        }
	    		
        public void HandleInput(InputEvent e)
		{
			e.Listener = this;

#if MULTITHREAD
			lock (input)
			{
			    input.Enqueue(e);
			}
			inputEvent.Set();
#else
			e.Dispatch();
#endif
        }

        public void Update()
		{
			AbstractEvent e = null;
            lock (events)
            {
                if (events.Any())
                    e = events.Dequeue();
            }

		    if (e != null)
		        onSendEvent(e);

#if MULTITHREAD
			inputEvent.Set();

            lock (exceptions)
            {
                if (!exceptions.Any())
                    return;
                var e = exceptions.Dequeue();

                throw e;
            }
#endif
		}

	    public void Validate(Point p, string id, ElementLayer layer)
	    {
            var cell = level.GetCell(p);

	        if (cell == null)
	            throw new Match3Exception("No Cell for {0}", p);

	        var el = cell.GetElement(layer);

	        if (el == null)
	            throw new Match3Exception("element for {0} is not found!", layer);

	        if (el.Id != id)
	            throw new Match3Exception("ID Mismatch! {0} != {1}", id, el.Id);

	        if (p != cell.Position)
	            throw new Match3Exception("cell position invalid {0} != {1}", cell.Position, p);
	    }

        public MatchInfo[] PossibleMatches
        {
            get
            {
                lock (possibleMatches)
                    return possibleMatches.ToArray();
            }
        }

		void IInputHandler.OnSwipe(Point a, Point b)
		{
		    if (level.CheckSwipe(a, b) != null)
		    {
		        var matches = level.Swipe(a, b);

		        DestroyMatches(matches);
		        EndTurn();
		    }
		    else
		    {
		        AddEvent(new SwipeFailedEvent(a, b));
		    }
		}

        void IInputHandler.OnBlast(IEnumerable<BlastData> directions)
		{
            AddEvent(level.HandleBlast(directions));
            EndTurn();
        }

		void IInputHandler.OnGetDirections(IEnumerable<BlastData> directions)
		{
            var cells = level.GetBlastCells(directions);

		    if (Collections.IsNullOrEmpty(cells))
		        return;

            AddEvent(new ShowDirectionsEvent(cells));
		}
    }
}