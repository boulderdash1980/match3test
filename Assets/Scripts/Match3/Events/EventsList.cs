﻿using System;
using System.Collections.Generic;

namespace match3.Events
{
    [Serializable]
    public sealed class EventsList : AbstractEvent
    {
        private readonly List<AbstractEvent> events = new List<AbstractEvent>();

        public EventsList(IEnumerable<AbstractEvent> events)
        {
            this.events.AddRange(events);
        }

        public void Add(AbstractEvent e)
        {
            if (e == null)
                return;

            events.Add(e);
        }

        public AbstractEvent[] Events
        {
            get { return events.ToArray(); }
        }
    }
}
