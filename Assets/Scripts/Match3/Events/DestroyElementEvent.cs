﻿using System;

namespace match3.Events
{
    [Serializable]
    public sealed class DestroyElementEvent : AbstractEvent
    {
        private readonly string target;

		public DestroyElementEvent(string target)
        {
            this.target = target;
        }

        public string Target
        {
            get { return target; }
        }
    }
}
