﻿using System;

using match3.Geom;

namespace match3.Events
{
    [Serializable]
    public sealed class GenerateElementEvent : AbstractEvent
    {
        private readonly int type;
        private readonly string id;

        private readonly Point cell;
        private readonly bool active;

        public GenerateElementEvent(string id, int type, bool active, Point cell)
        {
            this.id = id;
            this.type = type;
            this.cell = cell;
            this.active = active;
        }

        public int Type
        {
            get { return type; }
        }

        public string Id
        {
            get { return id; }
        }

        public Point Cell
        {
            get { return cell; }
        }

        public bool Active
        {
            get { return active; }
        }
    }
}