﻿using System;

namespace match3.Events
{
	[Serializable]
	public class GameEndEvent: AbstractEvent
	{
	    private readonly GameEndStatus status;

		public GameEndEvent(GameEndStatus status)
		{
		    this.status = status;
		}

		public GameEndStatus Status
		{
			get { return status; }
		}
	}
}

