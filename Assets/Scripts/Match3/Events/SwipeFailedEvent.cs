﻿using System;

using match3.Geom;

namespace match3.Events
{
    [Serializable]
    public sealed class SwipeFailedEvent : AbstractEvent
    {
        private readonly Point from;
        private readonly Point to;

        public SwipeFailedEvent(Point from, Point to)
        {
            this.from = from;
            this.to = to;
        }

        public Point From
        {
            get { return from; }
        }

        public Point To
        {
            get { return to; }
        }
    }
}
