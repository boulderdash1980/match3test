﻿using System;
using System.Collections.Generic;

using match3.Geom;

namespace match3.Events
{
    [Serializable]
    public sealed class ShuffleFieldEvent : AbstractEvent
    {
		private readonly Dictionary<string, Point> shuffle = new Dictionary<string, Point>();

		public void Add(string target, Point to)
		{
		    shuffle[target] = to;
		}

		public Dictionary<string, Point> Shuffle
        {
            get { return shuffle; }
        }
    }
}
