﻿using System;
using System.Collections.Generic;

using match3.Geom;

namespace match3.Events
{
    [Serializable]
    public sealed class DropElementEvent : AbstractEvent
    {
        private readonly List<Point> path = new List<Point>();
        private readonly string target;

        private readonly uint start;

        private readonly bool portalIn;
        private readonly bool portalOut;

        public DropElementEvent(string target, uint start, IEnumerable<Point> path, bool portalIn, bool portalOut)
        {
            this.target = target;
            this.start = start;
            this.path.AddRange(path);
            this.portalIn = portalIn;
            this.portalOut = portalOut;
        }

        public string Target
        {
            get { return target; }
        }

        public Point[] Path
        {
            get { return path.ToArray(); }
        }

        public uint Start
        {
            get { return start; }
        }

        public bool PortalIn
        {
            get { return portalIn; }
        }

        public bool PortalOut
        {
            get { return portalOut; }
        }
    }
}
