﻿using System;
using System.Collections.Generic;

using match3.Info;

namespace match3.Events
{
    [Serializable]
    public sealed class ShowDirectionsEvent : AbstractEvent
    {
        private readonly List<BlastDirections> directions = new List<BlastDirections>();

        public ShowDirectionsEvent(IEnumerable<BlastDirections> directions)
        {
            this.directions.AddRange(directions);
        }

        public BlastDirections[] Drections
        {
            get { return directions.ToArray(); }
        }
    }
}
