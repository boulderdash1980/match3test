﻿using System;

using match3.Geom;

namespace match3.Events.Input
{
    [Serializable]
    public sealed class SwipeEvent : InputEvent
    {
        private readonly Point a;
        private readonly Point b;

        public SwipeEvent(Point a, Point b)
        {
            this.a = a;
            this.b = b;
        }

        public override void Dispatch()
        {
            if(listener != null)
                listener.OnSwipe(a, b);
        }
    }
}
