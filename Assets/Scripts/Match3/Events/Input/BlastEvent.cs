﻿using System;
using System.Collections.Generic;
using match3.GameData;

namespace match3.Events.Input
{
    [Serializable]
    public sealed class BlastEvent : InputEvent
    {
        private readonly List<BlastData> directions = new List<BlastData>();

        public BlastEvent(IEnumerable<BlastData> directions)
        {
            this.directions.AddRange(directions);
        }

        public override void Dispatch()
        {
            if(listener != null)
                listener.OnBlast(directions);
        }
    }
}
