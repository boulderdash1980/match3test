﻿using System;

namespace match3.Events.Input
{
    [Serializable]
    public abstract class InputEvent
    {
        [NonSerialized]
        protected IInputHandler listener;

        public IInputHandler Listener
        {
            get { return listener; }
            set { listener = value; }
        }

        public abstract void Dispatch();
    }
}