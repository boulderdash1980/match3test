﻿using System;

using match3.GameData.Conditions;

namespace match3.Events
{
	[Serializable]
	public class WinConditionEvent: AbstractEvent
	{
		private readonly AbstractConditionData data;
		private readonly int id;
		private readonly float progress;

		public WinConditionEvent(int id, float progress, AbstractConditionData data)
		{
			this.data = data;
			this.id = id;
			this.progress = progress;
		}

		public AbstractConditionData Data
		{
			get { return data; }
		}

		public int Id
		{
			get { return id; }
		}

		public float Progress
		{
			get { return progress; }
		}
	}
}

