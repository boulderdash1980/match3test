﻿using System;
using System.Collections.Generic;

namespace match3.Events
{
    [Serializable]
    public sealed class EventsSequence : AbstractEvent
    {
        private readonly List<AbstractEvent> events = new List<AbstractEvent>();

        public EventsSequence()
        {
        }

        public EventsSequence(IEnumerable<AbstractEvent> events)
        {
            this.events.AddRange(events);
        }

        public void Append(AbstractEvent e)
        {
			if(e == null)
				return;
			
            events.Add(e);
        }

        public AbstractEvent[] Events
        {
            get { return events.ToArray(); }
        }
    }
}
