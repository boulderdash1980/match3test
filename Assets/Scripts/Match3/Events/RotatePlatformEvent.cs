﻿using System.Collections.Generic;
using System;

using match3.Geom;

namespace match3.Events
{
    [Serializable]
    public struct RotateElements
    {
        public string element;
        public Point to;

        public RotateElements(string element, Point to)
        {
            this.element = element;
            this.to = to;
        }
    }

    [Serializable]
	public sealed class RotatePlatformEvent : AbstractEvent
	{
	    private readonly int platform;
	    private readonly int angle;

	    private readonly List<RotateElements> elements = new List<RotateElements>();       

		public RotatePlatformEvent(int platform, int angle, IEnumerable<RotateElements> elements)
		{
			this.platform = platform;
			this.angle = angle;
            this.elements.AddRange(elements);
		}

		public int Platform
		{
			get { return platform; }
		}
			
		public int Angle
		{
			get { return angle; }
		}

        public RotateElements[] Elements
        {
            get { return elements.ToArray(); }
        }
	}
}