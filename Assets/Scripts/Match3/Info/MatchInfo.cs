﻿using System;
using System.Collections.Generic;

namespace match3.Info
{
    [Serializable]
    public sealed class MatchInfo
    {
        private readonly List<string> elements = new List<string>();

        private readonly int priority;

        public MatchInfo(int priority, IEnumerable<string> elements)
        {
            this.elements.AddRange(elements);
            this.priority = priority;
        }

        public MatchInfo(int priority, params string[] elements)
        {
            this.elements.AddRange(elements);
            this.priority = priority;
        }

        public string[] Elements
        {
            get { return elements.ToArray(); }
        }

        public int Priority
        {
            get { return priority; }
        }
    }
}
