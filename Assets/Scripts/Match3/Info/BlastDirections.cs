﻿
using System;
using System.Collections.Generic;

using match3.Geom;

namespace match3.Info
{
    [Serializable]
    public sealed class BlastDirections
    {
        private readonly string id;
        private readonly List<Point> cells = new List<Point>();

        public BlastDirections(string id, IEnumerable<Point> cells)
        {
            this.id = id;
            this.cells.AddRange(cells);
        }

        public Point[] Cells
        {
            get { return cells.ToArray(); }
        }

        public string Id
        {
            get { return id; }
        }
    }
}
