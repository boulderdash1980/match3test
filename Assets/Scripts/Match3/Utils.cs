﻿using System.Collections.Generic;
using System.Linq;
using System;

namespace match3.Utils
{
    internal static class Collections
    {
        public static bool IsNullOrEmpty<T>(ICollection<T> collection)
        {
            return collection == null || !collection.Any();
        }
	}

    internal static class RandomHelper
    {
        private static readonly Random Random = new Random();

        public static double NextDouble()
        {
            return Random.NextDouble();
        }

        public static int Next()
        {
            return Random.Next();
        }

        public static int Next(int a, int b)
        {
            return Random.Next(a, b);
        }
    }

    internal static class MathHelper
    {
        public static double NormalizedAngle(double a)
        {
            if (a >= 360)
                a -= 360;

            if (a < 0)
                a += 360;

            return a;
        }

        public static float NormalizedAngle(float a)
        {
            return (float)NormalizedAngle((double)a);
        }

        public static int NormalizedAngle(int a)
        {
            return (int)NormalizedAngle((double)a);
        }
    }
}

