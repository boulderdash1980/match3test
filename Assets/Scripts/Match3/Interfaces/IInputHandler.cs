﻿using System.Collections.Generic;

using match3.GameData;
using match3.Geom;

namespace match3
{
    public interface IInputHandler
    {
        void OnSwipe(Point a, Point b);
        void OnBlast(IEnumerable<BlastData> directions);
        void OnGetDirections(IEnumerable<BlastData> directions);
    }
}
