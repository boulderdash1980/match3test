﻿using System;

namespace match3.Exceptions
{
    public sealed class Match3Exception : Exception
    {
        public Match3Exception(string message) : base(message)
        {

        }

        public Match3Exception(string message, params object[] args) : base(string.Format(message, args))
        {
            
        }
    }
}
