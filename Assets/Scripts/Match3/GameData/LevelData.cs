using System;
using System.Collections.Generic;
using System.Linq;

using match3.GameData.Conditions;
using match3.Geom;

namespace match3.GameData
{
	[Serializable]
	public struct ProbabilityInfo
	{
		public int Lives;

		public KeyValuePair<int, int>[] Percentages;
	}

	[Serializable]
	public struct MatchStatement
	{
		public enum Statement { Match, PowerElement, Count };

		public int Moves;

		public int[] Statements;
	}

	[Serializable]
	public sealed class ProbabilitySchema
	{
		private List<ProbabilityInfo> config = new List<ProbabilityInfo>();
		private List<MatchStatement> statements = new List<MatchStatement>();

		private List<int> elements = new List<int>();

		public List<int> Elements
		{
			get { return elements; }
			set { elements = value; }
		}

		public List<ProbabilityInfo> Config
		{
			get { return config; }
			set { config = value; }
		}

		public List<MatchStatement> Statements
		{
			get { return statements; }
			set { statements = value; }
		}
	}

	[Serializable]
	public sealed class LevelData
	{
		private ProbabilitySchema probability = new ProbabilitySchema();
        private GameConditionData gameCondition = new GameConditionData();

        private List<CellData> cells = new List<CellData>();
		private List<PlatformData> platforms = new List<PlatformData>();

		private int colls;
		private int rows;

        [NonSerialized]
		private readonly Dictionary<Point, CellData> field = new Dictionary<Point, CellData>();

		public int Colls
		{
			get { return colls; }
			set { colls = value; Resize(); }
		}

		public int Rows
		{
			get { return rows; }
			set { rows = value; Resize(); }
		}

		public int Cell
		{
			get; set;
		}
			
		public int ScoreBronze
		{
			get; set;
		}

		public int ScoreSilver
		{
			get; set;
		}

		public int ScoreGold
		{
			get; set;
		}

		public List<CellData> Cells
		{
			get { return cells; }
            set { cells = value; }
		}

		public ProbabilitySchema Probability
		{
			get { return probability; }
			set { probability = value; }
		}

		public List<PlatformData> Platforms
		{
			get { return platforms; }
			set { platforms = value; }
		}

		public GameConditionData GameCondition
		{
			get { return gameCondition; }
			set { gameCondition = value; }
		}
			
	    public CellData GetCell(Point p)
	    {
	        if (!field.Any())
	            Resize();

	        if (field.ContainsKey(p))
	            return field[p];

	        var cell = new CellData {Point = p};

            field[p] = cell;
            cells.Add(cell);

			return cell;
		}

		private void Resize()
		{
		    cells.RemoveAll(e => e.Point.x >= rows || e.Point.y >= colls);

            field.Clear();

            cells.ForEach(e => field[e.Point] = e);
		}
	}
}