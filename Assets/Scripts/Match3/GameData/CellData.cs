﻿using System.Collections.Generic;
using System;

using match3.Geom;

namespace match3.GameData
{
    [Serializable]
    public struct ElementInfo
    {
        public string Id;

        public ElementLayer Layer;
        public int Element;
    }

    [Serializable]
    public sealed class CellData
    {
        //common props
        public Point Point;
        public Point Gravity = new Point(0, 1);

        public List<ElementInfo> Elements = new List<ElementInfo>();
        
        //disabled(invisible)
        public bool Disabled;

        //generate elements
        public bool Emitter;

        //portal
        public CellData portal;
    }
}
