﻿using System.Linq;
using System;

using match3.Exceptions;
using match3.Model;

namespace match3.GameData
{
    [Serializable]
    public sealed class MatchData
    {
        private int[] vertices;

        private string name;

        private int spawnElement = Element.None;

        private int rows, colls;

		private bool ignoreIfNoMatchesFound;

        private int priority;

        public MatchData()
        {

        }

        public MatchData(int rows, int colls)
        {
            this.rows = rows;
            this.colls = colls;

            Resize();
        }

        private int Index(int row, int coll)
        {
            return row * colls + coll;
        }

        private void Resize()
        {
            Array.Resize(ref vertices, rows * colls);
        }

        private bool IndexValid(int row, int coll)
        {
            if (vertices == null)
                return false;

            var i = Index(row, coll);

            return i >= 0 && i < vertices.Length;
        }
        
        public int this[int row, int coll]
        {
            get
            {
                if (!IndexValid(row, coll))
                    throw new Match3Exception("Invalid index {0}, {1}, rows = {2}, colls {3}", row, coll, rows, colls);

                return vertices[Index(row, coll)];
            }
            set
            {
                if (!IndexValid(row, coll))
                    throw new Match3Exception("Invalid index {0}, {1}, rows = {2}, colls {3}", row, coll, rows, colls);

                vertices[Index(row, coll)] = value;
            }
        }

        public int Rows
        {
            get { return rows; }
            set
            {
                rows = value;
                Resize();
            }
        }

        public int Colls
        {
            get { return colls; }
            set
            {
                colls = value;
                Resize();
            }
        }

        public int Priority
        {
            get { return priority; }
            set { priority = value; }
        }
        
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int SpawnElement
        {
            get { return spawnElement; }
            set { spawnElement = value; }
        }

		public bool IgnoreIfNoMatchesFound
		{
			get { return ignoreIfNoMatchesFound; }
			set { ignoreIfNoMatchesFound = value; }
		}

        //only for serialization
        public int[] Vertices
        {
            get { return vertices; }
            set { vertices = value; }
        }

        public int MatchCount
        {
            get { return vertices.Count(v => v == 1); }
        }

        public bool Equals(MatchData item)
        {
            if (vertices == null || item == null || item.vertices == null)
                return false;

            if (item.name != name || item.colls != colls || item.rows != rows)
                return false;

            return vertices.SequenceEqual(item.vertices);
        }

        public MatchData Clone()
        {
            var item = new MatchData(Rows, Colls)
            {
                Name = name,
                SpawnElement = spawnElement,
                IgnoreIfNoMatchesFound = ignoreIfNoMatchesFound,
                Priority = priority,
            };

            Array.Copy(vertices, item.vertices, vertices.Length);

            return item;
        }

        public MatchData Inverse()
        {
            var item = new MatchData(Rows, Colls)
            {
                Name = name,
                SpawnElement = spawnElement,
                IgnoreIfNoMatchesFound = ignoreIfNoMatchesFound,
                Priority = priority,
            };

            for (var x = 0; x < rows; ++x)
                for (var y = 0; y < colls; ++y)
                    item[x, y] = this[rows - 1 - x, y];

            return item;
        }

        public MatchData Crop()
        {
            int mx = -1, my = -1, rows = 0, colls = 0;

            for (var y = 0; y < this.colls; ++y)
            {
                var r = 0;

                for (var x = 0; x < this.rows; ++x)
                {
                    if (this[x, y] == 0)
                        continue;

                    mx = mx == -1 || x < mx ? x : mx;
                    my = my == -1 || y < my ? y : my;

                    ++r;
                }

                rows = Math.Max(r, rows);
                colls = r > 0 ? colls + 1 : colls;
            }

            var data = new MatchData(rows, colls)
            {
                Name = name,
                SpawnElement = spawnElement,
                IgnoreIfNoMatchesFound = ignoreIfNoMatchesFound,
                Priority = priority,
            };

            for (var y = my; y < my + colls; ++y)
                for (var x = mx; x < mx + rows; ++x)
                    data[x - mx, y - my] = this[x, y];

            return data;
        }

        public override string ToString()
        {
            var s = string.Format("{0} Rows {1}, Colls {2} \n", Name, rows, colls);

            for (var y = 0; y < colls; ++y)
            {
                for (var x = 0; x < rows; ++x)
                {
                    if (this[x, y] == 0)
                        continue;

                    s += string.Format("x : {0}, y : {1}, ", x, y);
                }
            }

            return s;
        }
    }
}

