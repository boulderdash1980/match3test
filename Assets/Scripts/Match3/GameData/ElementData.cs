﻿using System.Collections.Generic;
using System;

using match3.Geom;
using match3.Model;

namespace match3.GameData
{
    [Serializable]
    public sealed class ElementData
    {
        //common props
        public ElementLayer Layer;

        public int Type = Element.None;

        public int ScoreBonus;

        public bool Generated;
        public bool ApplyVirus;
        public bool Droppable;
        public bool Selectable;

        public bool IgnoreShuffle;
        //

        //blocker strategy
        public bool BlockMove;
        public bool BlockCombination;
		public bool BlockGeneration;
		public bool BlockMatch;
        public bool UnBreakable;
        //

        //destroy strategy
        public bool DestroyOnMatchNear;
        public bool DestroyOnBlastNear;
        public bool DestroyElementsUnder;

        public int SpawnOnDestroy = Element.None;
        public int SpawnOnDestroyChance;
        //

        //match element relation
        public List<int> Relation = new List<int>();
        //

        //virus strategy
        public bool Virus;

        //uniq element virus property
        public int SpawnOnUpgradeVirus = Element.None;

        //common propertys for virus family
        public int VirusTurns;
        public int VirusFamily = Element.None;
        public VirusStrategy VirusStrategy;
        //
        
        //blast strategy
        public bool ActivateOnInput;
		public List<Point> BlastDirections = new List<Point>();
        public List<Point> InputDirecitons = new List<Point>();

        public int BlastWidth;
        public int BlastHeight;
        //
    }
}
