﻿using System;
using System.Collections.Generic;

namespace match3.GameData.Conditions
{
	[Serializable]
	public sealed class GameConditionData
	{
		private AbstractConditionData loseCondition;

	    private List<WinConditionData> winConditions = new List<WinConditionData>();

		private bool finishAfterAllWins = true;

		public AbstractConditionData LoseCondition
		{
			get { return loseCondition; }
			set { loseCondition = value; }
		} 

		public List<WinConditionData> WinConditions
		{
			get { return winConditions; }
			set { winConditions = value; }
		}

		public bool FinishAfterAllWins
		{
			get { return finishAfterAllWins; }
			set { finishAfterAllWins = value; }
		}
	}
}
