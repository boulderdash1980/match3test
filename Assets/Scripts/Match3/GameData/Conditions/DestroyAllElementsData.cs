﻿using System;

namespace match3.GameData.Conditions
{
	[Serializable]
	public sealed class DestroyAllElementsData: WinConditionData
	{
        public int ElementType
        {
            get; set;
        }
    }
}

