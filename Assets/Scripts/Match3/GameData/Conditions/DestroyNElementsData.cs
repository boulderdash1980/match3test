﻿using System;

namespace match3.GameData.Conditions
{
	[Serializable]
	public sealed class DestroyNElementsData : WinConditionData
    {
	    public int ElementType
	    {
	        get; set;
	    }

	    public int Count
	    {
	        get; set;
	    }
	}
}

