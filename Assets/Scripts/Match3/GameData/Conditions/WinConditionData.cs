﻿using System;

namespace match3.GameData.Conditions
{
	[Serializable]
	public abstract class WinConditionData: AbstractConditionData
	{
	    public bool Required
	    {
	        get; set;
        }
	}
}

