﻿using System;

namespace match3.GameData.Conditions
{
	[Serializable]
	public sealed class MovesEndsData : LoseConditionData
    {
	    public int Moves
	    {
	        get; set;
        }
	}
}

