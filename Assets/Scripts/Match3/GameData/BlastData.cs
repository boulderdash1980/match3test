﻿using System;

using match3.Geom;

namespace match3.GameData
{
    [Serializable]
    public struct BlastData
    {
        public Point Cell;
        public Point Direction;

        public BlastData(Point p, Point direction)
        {
            Cell = p;
            Direction = direction;
        }
    }
}
