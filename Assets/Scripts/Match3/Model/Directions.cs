﻿
using match3.Geom;

namespace match3.Model
{
    internal sealed class Directions
    {
        public static Point Up
        {
            get { return new Point(0, -1); }
        }

        public static Point Down
        {
            get { return new Point(0, 1); }
        }

        public static Point Left
        {
            get { return new Point(-1, 0); }
        }

        public static Point Right
        {
            get { return new Point(1, 0); }
        }

        public static Point LeftUp
        {
            get { return new Point(-1, -1); }
        }

        public static Point RightUp
        {
            get { return new Point(1, -1); }
        }

        public static Point LeftDown
        {
            get { return new Point(-1, 1); }
        }

        public static Point RightDown
        {
            get { return new Point(1, 1); }
        }

        public static Point Center
        {
            get { return Point.Zero; }
        }
    }
}
