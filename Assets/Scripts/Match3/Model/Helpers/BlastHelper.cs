﻿using System.Collections.Generic;
using System.Linq;

using match3.Geom;
using match3.Events;
using match3.GameData;
using match3.Info;

using match3.Utils;

namespace match3.Model.Helpers
{
    internal sealed class BlastHelper : AbstractHelper
    {
        private readonly Dictionary<Element, Point> directions = new Dictionary<Element, Point>();
        private readonly List<Element> blastElements = new List<Element>();

        private readonly DestroyHelper destroyHelper;

        public BlastHelper(Level level, DestroyHelper destroyHelper) : base(level)
        {
            this.destroyHelper = destroyHelper;
        }

        private int BlastCellsCount(Cell start, int width, Point dir)
        {
            var cells = GetCellsInDirection(start, width, dir);

            return cells.Count(e => e.IsOccupied());
        }

        private IEnumerable<Cell> GetCellsInDirection(Cell start, int width, Point dir)
        {
            var cells = new List<Cell>();

            if (start == null)
                return cells;

            var node = start.GetNeighbour(dir);

            var w = 0;

            while (node != null && w++ < width)
            {
                if (node.IsBlastBlocked())
                    break;

                cells.Add(node);

                node = level.GetNeighbour(node, dir);
            }

            return cells;
        }

        private IEnumerable<Point> GetRotatedDirections(Element elem, int angle)
        {
            var first = elem.BlastDirections.First();
            var rotate = angle - first.Angle();

            return elem.BlastDirections.Select(p => Point.Rotate(p, rotate));
        }

        private IEnumerable<Point> GetDefaultDirection(Element elem, Element prev)
        {
            var input = elem.InputDirections;

            if (!input.Any())
                return elem.BlastDirections;

            var directions = new Dictionary<Point, IEnumerable<Point>>();

            var delta = elem.Cell.Position - prev.Cell.Position;
            var angle = delta.Angle();

            foreach (var dir in elem.InputDirections)
            {
                var a = dir.NormalizedAngle();

                if (angle == a || MathHelper.NormalizedAngle(angle - 180) == a)
                    continue;

                directions[dir] = GetRotatedDirections(elem, a).ToArray();
            }

            var keys = directions.Keys.OrderByDescending(e => BlastCellsCount(elem.Cell, elem.BlastWidth, e));

            return directions[keys.First()];
        }

        private IEnumerable<Point> GetElementDirection(Element elem, Point dir)
        {
            if (dir == Point.Zero)
                return elem.BlastDirections.Select(e => Point.Zero);

            return GetRotatedDirections(elem, dir.Angle());
        }

        private IEnumerable<Cell> GetBlastCells(Element elem, Element prev)
        {
            var cells = new HashSet<Cell>();

            var points = directions.ContainsKey(elem) ? GetElementDirection(elem, directions[elem]) : GetDefaultDirection(elem, prev);

            foreach (var dir in points)
            {
                if (dir == Point.Zero)
                    continue;

                var h = elem.BlastHeight / 2;

                for (var i = -h; i <= h; ++i)
                {
                    var cell = elem.Cell.Position + Point.Rotate(new Point(0, i), dir.Angle());

                    cells.AddRange(GetCellsInDirection(level.GetCell(cell), elem.BlastWidth, dir));
                }
            }

            return cells;
        }

        private IEnumerable<KeyValuePair<Element, List<Cell>>> GetAllBlastCells(Element elem, Element prev)
        {
            var list = new List<KeyValuePair<Element, List<Cell>>>();
            var cells = new List<Cell> { elem.Cell };
            
            var directions = GetBlastCells(elem, prev);

            blastElements.Add(elem);

            list.Add(new KeyValuePair<Element, List<Cell>>(elem, cells));

            foreach (var cell in directions)
            {
                var front = cell.GetFrontElement();

                if (front != null && front.ActivateOnInput && !blastElements.Contains(front))
                {
                    list.AddRange(GetAllBlastCells(front, elem));

                    continue;
                }

                cells.Add(cell);
            }

            return list;
        }

        private Element HandleInput(IEnumerable<BlastData> directions)
        {
            var data = directions as BlastData[] ?? directions.ToArray();

            if(Collections.IsNullOrEmpty(data))
                return null;

            blastElements.Clear();

            this.directions.Clear();

            foreach (var d in data)
            {
                var cell = level.GetCell(d.Cell);

                if (cell == null)
                    return null;

                var front = cell.GetFrontElement();

                if (front == null || !front.ActivateOnInput)
                    return null;

                this.directions[front] = d.Direction;
            }

            var first = this.directions.First();

            return first.Key;
        }

        public AbstractEvent HandleBlast(IEnumerable<BlastData> directions)
        {
            var elem = HandleInput(directions);

            if (elem == null)
				return null;
				
            var blast = GetAllBlastCells(elem, null);
            var events = new List<AbstractEvent>();
            var neighbours = new List<Cell>();

            foreach (var kvp in blast)
            {
                var cells = kvp.Value;

                events.Add(destroyHelper.DestroyElements(cells));

                foreach (var cell in cells)
                {
                    var list = level.GetNeighbours(cell, Directions.Left, Directions.Right, Directions.Down, Directions.Up);

                    neighbours.AddRange(from n in list let front = n.GetFrontElement() where front != null && front.DestroyOnBlastNear select n);
                }

                events.Add(destroyHelper.DestroyFrontElements(neighbours));

                neighbours.Clear();
            }

            return new EventsList(events);
        }

        public BlastDirections[] GetBlastDirections(IEnumerable<BlastData> directions)
        {
            var elem = HandleInput(directions);

            if (elem == null)
				return null;

			var blast = GetAllBlastCells(elem, null);

            return blast.Select(kvp => new BlastDirections(kvp.Key.Id, kvp.Value.Select(e => e.Position))).ToArray();
        }
    }
}
