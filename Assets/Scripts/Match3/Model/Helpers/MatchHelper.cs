﻿using System;
using System.Collections.Generic;
using System.Linq;

using match3.GameData;
using match3.Geom;

namespace match3.Model.Helpers
{
    internal sealed class MatchHelper : AbstractHelper
    {
        private readonly List<MatchData> matches = new List<MatchData>();

        //for avoid allocations in every find match request
        private readonly List<Element> match = new List<Element>();

        public MatchHelper(Level level, IEnumerable<MatchData> matches) : base(level)
        {
            Action<MatchData> addMatch = data =>
            {
                //check is uniq
                var find = this.matches.Find(e => e.Equals(data));

                if (find == null)
                    this.matches.Add(data);
            };

            var i = 0;

            foreach (var match in matches)
            {
                var item = match;

                item.Priority = i++;
                
                var axis = new Point(match.Colls - 1, 0);

                //rotate four angles
                for (var n = 0; n < 4; ++n)
                {
                    var next = new MatchData(item.Rows, item.Colls)
                    {
                        SpawnElement = item.SpawnElement,
                        Name = item.Name,
                        Priority = item.Priority,
						IgnoreIfNoMatchesFound = item.IgnoreIfNoMatchesFound
                    };

                    for (var x = 0; x < item.Rows; ++x)
                    {
                        for (var y = 0; y < item.Colls; ++y)
                        {
                            if (item[x, y] == 0)
                                continue;

                            var pt = new Point(x, y) - axis;

                            next[pt.y, -pt.x] = 1;
                        }
                    }

                    //crop
                    item = item.Crop();
                    addMatch(item);

                    //inverse
                    item = item.Inverse();
                    addMatch(item);

                    item = next;
                }
            }
        }

        private Element GetMatchElement(Point p)
        {
            var cell = level.GetCell(p.x, p.y);

            if (cell == null || cell.Disabled)
                return null;

            var el = cell.GetElement(ElementLayer.GamePlay);

            if (el == null || !el.CanMatch() || cell.IsMatchBlocked())
                return null;

            return el;
        }

        private Match GetMatch(int x, int y, MatchData data, Element elem)
        {
            match.Clear();

			for (var my = 0; my < data.Colls; my++)
			{
				for (var mx = 0; mx < data.Rows; mx++)
				{
					var x2 = x + mx;
					var y2 = y + my;

					if (data[mx, my] != 1)
						continue;

				    var el = GetMatchElement(new Point(x2, y2));

				    if (el == null || !el.CanMatch(elem))
				        return null; 

                    match.Add(el);
                }
            }

            return match.Any() ? new Match(match, data.Priority, data.SpawnElement, elem.Cell.Position) : null;
        }

		private Match CheckMatch(int x, int y, MatchData data, Element elem)
        {
            var endX = Math.Max(0, x - data.Rows + 1);
            var endY = Math.Max(0, y - data.Colls + 1);

            for (var offY = y; offY >= endY; offY--)
            {
                for (var offX = x; offX >= endX; offX--)
                {
                    var match = GetMatch(offX, offY, data, elem);

                    if (match != null)
                        return match;
                }
            }

            return null;
        }

        public Match FindMatch(int x, int y)
        {
            var element = GetMatchElement(new Point(x, y));

            if (element == null)
                return null;

			Match match = null;

			foreach (var m in matches)
			{
				if (match == null && m.IgnoreIfNoMatchesFound)
					continue;

                var newMatch = CheckMatch(x, y, m, element);
				if (newMatch != null)
					match = newMatch;
			}

			return match;
        }

        public Match FindMatch(Point p)
        {
            return FindMatch(p.x, p.y);
        }

        public int MinMatchLenght()
        {
            if (!matches.Any())
                return 0;

            var order = matches.OrderBy(m => m.MatchCount);

            return order.First().MatchCount;
        }

        public MatchData[] GetMatchesWithLenght(int lenght)
        {
            return matches.Where(m => m.MatchCount <= lenght).ToArray();
        }
    }
}
