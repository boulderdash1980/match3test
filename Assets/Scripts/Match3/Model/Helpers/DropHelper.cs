﻿using System.Collections.Generic;
using System.Linq;

using match3.Events;
using match3.Geom;

namespace match3.Model.Helpers
{
    internal sealed class DropHelper : AbstractHelper
    {
        private class MovementEntry
        {
            public Element Element;

            public uint Tick;
            public readonly List<KeyValuePair<uint, Point>> Path = new List<KeyValuePair<uint, Point>>();

            public bool PortalIn;
            public bool PortalOut;

            public AbstractEvent ToEvent()
            {
                return new DropElementEvent(Element.Id, Tick, Path.Select(p => p.Value), PortalIn, PortalOut);
            }
        }

        private readonly List<Cell> gravityChainEnds = new List<Cell>();

        private uint tick;

		private readonly List<MovementEntry> movements = new List<MovementEntry>();

        private readonly List<AbstractEvent> drop = new List<AbstractEvent>();
        private readonly List<AbstractEvent> generation = new List<AbstractEvent>();

        //in out portals cells
        private readonly List<Cell> portalsIn = new List<Cell>();
        private readonly List<Cell> portalsOut = new List<Cell>();

        private readonly HashSet<Element> changedElements = new HashSet<Element>();
        
        public DropHelper(Level level) : base(level)
        {
            InitializeGravity();
        }

        private void InitializeGravity()
        {
            for (var x = 0; x < level.Width; ++x)
            {
                for (var y = 0; y < level.Height; ++y)
                {
                    var cell = level.GetCell(x, y);

                    if (cell == null || cell.Disabled)
                        continue;

                    //adding in out cells if cell portal
                    if(cell.PortalLink != null)
                    {
                        portalsIn.Add(cell);
                        portalsOut.Add(cell.PortalLink);
                    }

                    //cell teleports to other or drop from near
                    var to = cell.PortalLink ?? cell.GetNeighbour(cell.Gravity);

                    if (to == null || to.Disabled)
                    {
                        gravityChainEnds.Add(cell);
                        continue;
                    }

                    to.GravityFrom = cell.Position;
                }
            }
        }

        private bool SpawnToCell(Cell cell)
        {
            if (cell.Disabled || !cell.Emitter || cell.IsOccupied())
                return false;

            var element = level.GenerateElement(cell);

            element.InMovement = true;
            changedElements.Add(element);

            generation.Add(new GenerateElementEvent(element.Id, element.Type, false, cell.Position));

            //make drop event to spawn cell
            var entry = new MovementEntry
            {
                Tick = tick,
                Element = element,
                PortalIn = portalsIn.Contains(cell),
                PortalOut = portalsOut.Contains(cell)
            };

            entry.Path.Add(new KeyValuePair<uint, Point>(tick, cell.Position));

            movements.Add(entry);
            level.OnGenerateElement(element);

            return true;
        }

        private void Cleanup()
        {
			foreach (var e in changedElements)
				e.InMovement = false;

			changedElements.Clear();
        }

        private bool TryMakeMovement(Cell from, Cell to)
        {
            if (from == null || from.Disabled || to == null || to.Disabled)
                return false;

            if (from.IsMoveElements())
                return false;

            var items = from.GetDropElements();

			if(!items.Any())
				return false;

			foreach(var el in items)
			{
                var entry = movements.Find(e => e.Element == el);

                if(entry == null)
                {
                    entry = new MovementEntry
                    {
                        Tick = tick,
                        Element = el,
                        PortalIn = portalsIn.Contains(from),
                        PortalOut = portalsOut.Contains(to)
                    };

                    movements.Add(entry);
                }

                if (entry.Path.Any())
                {
                    while (entry.Path.Last().Key + 1 < tick)
                    {
                        var last = entry.Path.Last();
                        entry.Path.Add(new KeyValuePair<uint, Point>(last.Key + 1, last.Value));
                    }
                }

                entry.Path.Add(new KeyValuePair<uint, Point>(tick, to.Position));

                if(entry.PortalIn)
                {
                    movements.Remove(entry);
                    drop.Add(entry.ToEvent());
                }

			    el.InMovement = true;
                changedElements.Add(el);
                
                from.RemoveElement(el);
	            to.AddElement(el);
			}

            return true;
        }

        public bool StraightDropStep()
        {
            var changed = false;

            foreach (var gend in gravityChainEnds)
            {
                for (var cell = gend; cell != null; cell = level.GetCell(cell.GravityFrom))
                {
                    if (cell.Disabled || cell.IsOccupied())
                        continue;

                    if (SpawnToCell(cell))
                    {
                        changed = true;
                    }
                    else
                    {
                        var from = level.GetCell(cell.GravityFrom);
                        changed |= TryMakeMovement(from, cell);
                    }
                }
            }
            return changed;
        }

        public bool DiagonalDropStep()
        {
            var changed = false;
            var semaphore = false;

            foreach (var gend in gravityChainEnds)
            {
                for (var cell = gend; cell != null; cell = level.GetCell(cell.GravityFrom))
                {
                    if (cell.Disabled || cell.IsOccupied())
                        continue;

                    var from = level.GetCell(cell.GravityFrom);

                    if (!cell.HasDroppableUpper())
                    {
                        var ncells = new[] {
                            cell.GetNeighbour(new Point(-cell.Gravity.y, cell.Gravity.x)),
                            cell.GetNeighbour(new Point(cell.Gravity.y, -cell.Gravity.x))
                        };

                        var diagonals = new List<Cell>();

                        foreach (var nc in ncells)
                        {
                            if (nc == null)
                                continue;

                            if (!nc.IsOccupied() && !nc.Disabled)
                                continue;

                            var diag = nc.GetNeighbour(-cell.Gravity);
                            if (diag == null)
                                continue;

                            var onDiag = diag.GetNeighbour(-cell.Gravity);
                            if (onDiag != null && onDiag.IsDropable() && from != null && !from.IsOccupied())
                                continue;

                            if (diag.IsDropable())
                                diagonals.Add(diag);
                        }

                        if (diagonals.Any())
                        {
                            Cell selected;
                            if (diagonals.Count > 1)
                            {
                                selected = diagonals[semaphore ? 0 : 1];
                                semaphore = !semaphore;
                            }
                            else
                            {
                                selected = diagonals[0];
                            }

                            changed |= TryMakeMovement(selected, cell);
                        }
                    }

                    if (SpawnToCell(cell))
                        changed = true;
                }
            }

            return changed;
        }

        private IEnumerable<AbstractEvent> MakeEvents()
        {
            var events = new List<AbstractEvent>();

            events.AddRange(generation);
			events.AddRange(drop);
            events.AddRange(movements.Select(e => e.ToEvent()));

            return events;
        }

        public AbstractEvent Process()
        {
            movements.Clear();
            generation.Clear();

            var changed = true;

            for (tick = 0; changed; tick++)
            {
                changed = StraightDropStep();

                bool diagChanged;                    
                do
                {
                    diagChanged = DiagonalDropStep();
                    changed |= diagChanged;
                }
                while (diagChanged);

                Cleanup();
            }

            return new EventsList(MakeEvents());
        }
    }
}
