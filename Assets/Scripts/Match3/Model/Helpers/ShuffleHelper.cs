﻿using System.Collections.Generic;
using System.Linq;

using match3.GameData;
using match3.Events;
using match3.Utils;
using match3.Geom;

namespace match3.Model.Helpers
{
    internal sealed class ShuffleHelper : AbstractHelper
    {
        private readonly MatchHelper matchHelper;

        private readonly List<Element> elements = new List<Element>();
        private readonly List<Cell> cells = new List<Cell>();

        public ShuffleHelper(Level level, MatchHelper matchHelper) : base(level)
        {
            this.matchHelper = matchHelper;
        }

        private void ClearField()
        {
            elements.Clear();
            cells.Clear();

            for (var x = 0; x < level.WidthInCells; ++x)
            {
                for (var y = 0; y < level.HeightInCells; ++y)
                {
                    var cell = level.GetCell(x, y);

                    if (cell == null || cell.Disabled || cell.Empty)
                        continue;

                    var front = cell.GetFrontElement();

                    if (front.IgnoreShuffle)
                        continue;

                    front.RemoveFromParent();

                    elements.Add(front);
                    cells.Add(cell);
                }
            }
        }

        private List<KeyValuePair<int, int>> GetRelations()
        {
            var relations = new Dictionary<int, int>();
            var list = elements.ToList();

            while (list.Any())
            {
                var first = list.First();
                var relation = first.Type;

                relations[relation] = list.Count(e => e.Type == relation);

                list.RemoveAll(e => e.Type == relation);
            }

            var min = matchHelper.MinMatchLenght();
            
            //remove elements less then min match count
            relations.RemoveAll(e => e.Value < min);

            return relations.ToList();
        }

        private Cell GetCellForSwap(Cell from, ICollection<Cell> match)
        {
            var directions = new[] { Directions.Left, Directions.Right, Directions.Up, Directions.Down };

            return directions.Select(dir => level.GetNeighbour(from, dir)).Where(next => next != null).FirstOrDefault(next => cells.Contains(next) && !match.Contains(next));
        }

        private ICollection<Cell> FindMatchCells(MatchData data, int relation)
        {
            var list = cells.ToList();
            var match = new List<Cell>();

            var select = elements.Where(i => i.Type == relation).ToList();

            //return if not enought elements for match
            if (select.Count < data.MatchCount)
                return null;

            while (list.Any())
            {
                var first = list.First();
               
                match.Clear();

                for (var x = 0; x < data.Rows; ++x)
                {
                    for (var y = 0; y < data.Colls; ++y)
                    {
                        if (data[x, y] == 0)
                            continue;

                        var next = level.GetNeighbour(first, new Point(x, y));

                        if (!cells.Contains(next))
                            continue;

                        match.Add(next);
                    }
                }

                if (match.Count >= data.MatchCount)
                {
                    //find cell for swap
                    foreach (var cell in match)
                    {
                        var swap = GetCellForSwap(cell, match);

                        if (swap == null)
                            continue;

                        var item = elements.First(i => i.Type != relation);

                        swap.AddElement(item);

                        foreach (var c1 in match)
                        {
                            var el = select.First();

                            if (c1 != cell)
                            {
                                //check if place element not matching with other matches
                                var check = matchHelper.FindMatch(c1.Position);

                                if (check != null)
                                {
                                    el.RemoveFromParent();
                                    return null;
                                }
                            }

                            select.Remove(el);
                            c1.AddElement(el);
                        }

                        swap.SwapFrontElements(cell);

                        return new List<Cell>(match) { swap };
                    }
                }

                list.Remove(first);
            }

            return null;
        }

        private ICollection<Cell> MakePossibleMatch()
        {
            //find possible relations
            var relations = GetRelations();

            //return if no one relation
            if (!relations.Any())
                return null;

            //randomly place possible match
            do
            {
                var relation = relations.First();
                var matches = matchHelper.GetMatchesWithLenght(matchHelper.MinMatchLenght()).ToList();

                //randomly shuffle cells for any match
                cells.Shuffle();

                while (matches.Any())
                {
                    var first = matches.First();
                    var match = FindMatchCells(first, relation.Key);

                    matches.Remove(first);

                    if (Collections.IsNullOrEmpty(match))
                        continue;

                    foreach (var cell in match)
                    {
                        var front = cell.GetFrontElement();

                        elements.Remove(front);
                    }

                    cells.RemoveAll(c => match.Contains(c));

                    return match;
                }

                relations.Remove(relation);
            }
            while (Collections.IsNullOrEmpty(level.FindAllPossibleMatches()) && relations.Any());

            return null;
        }

        private bool RandomShuffle(List<Cell> cells, List<Element> elements)
        {
            cells.Shuffle();

            while (cells.Any())
            {
                var item = elements.First();
                var cell = cells.First();

                cell.AddElement(item);

                if (matchHelper.FindMatch(cell.Position) != null)
                {
                    item.RemoveFromParent();
                    return false;
                }

                cells.Remove(cell);
                elements.Remove(item);
            }

            return true;
        }

        public AbstractEvent Shuffle()
        {
            //clearing shuffle cells first
            ClearField();

            //make at lest one of 100% possible match
            var matched = MakePossibleMatch();

            //return if not any match
            if (Collections.IsNullOrEmpty(matched))
                return null;

            //random shuffle other elements
            while (!RandomShuffle(cells.ToList(), elements.ToList()))
                elements.ForEach(e => e.RemoveFromParent());

            cells.ForEach(e => matched.Add(e));

            //fill event
            var shuffle = new ShuffleFieldEvent();

            foreach (var cell in matched)
            {
                var front = cell.GetFrontElement();

                shuffle.Add(front.Id, cell.Position);
            }

            return shuffle;
        }
    }
}
