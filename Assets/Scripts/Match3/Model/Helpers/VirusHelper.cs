﻿using System.Collections.Generic;
using System.Linq;

using match3.Exceptions;
using match3.Events;
using match3.Utils;

namespace match3.Model.Helpers
{
    internal sealed class VirusHelper : AbstractHelper
    {
        private delegate AbstractEvent VirusHandler(int type);

        private struct VirusLogic
        {
            public VirusHandler Handler;

            public int Turns;
            public int Family;
        }

        private readonly Dictionary<int, List<Element>> virusSpawn = new Dictionary<int, List<Element>>();
        
        private readonly List<VirusLogic> virusStrategies = new List<VirusLogic>();

        private readonly Dictionary<VirusStrategy, VirusHandler> handlers = new Dictionary<VirusStrategy, VirusHandler>();

        private readonly HashSet<int> destroyOnLastTurn = new HashSet<int>();

        public VirusHelper(Level level) : base(level)
        {
            level.onGenerateElementEvent += OnGenerateElementEvent;
            level.onDestroyElementEvent += OnElementEventDestroyed;

            handlers[VirusStrategy.Expand] = HandleExpand;
            handlers[VirusStrategy.Upgrade] = HandleUpgrade;
            handlers[VirusStrategy.ExpandAndUpgrade] = HandleUpgradeOrExpand;
        }

        private void OnGenerateElementEvent(Element e)
        {
            if (!e.Virus)
                return;

            var family = e.VirusFamily;

            if(family == Element.None)
                throw new Match3Exception("Invalid VirusFamily {0}", family);

            if (!virusSpawn.ContainsKey(family))
            {
                virusSpawn[family] = new List<Element>();

                var strategy = new VirusLogic
                {
                    Turns = e.VirusTurns,
                    Family = family,
                    Handler = handlers[e.VirusStrategy]
                };

                virusStrategies.Add(strategy);
            }

            virusSpawn[family].Add(e);
        }

        private void OnElementEventDestroyed(Element e)
        {
            if(!e.Virus)
                return;

            var family = e.VirusFamily;

            if (family == Element.None)
                throw new Match3Exception("Invalid VirusFamily {0}", family);

            if (!virusSpawn.ContainsKey(family))
                return;

            var list = virusSpawn[family];

            list.Remove(e);

            if (!list.Any())
            {
                virusSpawn.Remove(family);
                virusStrategies.RemoveAll(s => s.Family == family);
            }

            destroyOnLastTurn.Add(family);
        }

        private AbstractEvent HandleExpand(int family)
        {
            var elements = virusSpawn[family];

            var cells = elements.Select(e => e.Cell);
            var neighbours = new HashSet<Cell>();

            foreach (var cell in cells)
            {
                var list = level.GetNeighbours(cell, Directions.Left, Directions.Right, Directions.Up, Directions.Down);

                foreach (var n in list)
                {
                    var el = n.GetFrontElement();

                    if (el == null || !el.ApplyVirus)
                        continue;

                    neighbours.Add(n);
                }
            }

            if (!neighbours.Any())
                return null;

            var r = RandomHelper.Next(0, neighbours.Count - 1);

            var target = neighbours.ElementAt(r);
            var sequence = new EventsSequence();

            var front = target.GetFrontElement();

            front.RemoveFromParent();

            var virus = level.GenerateElement(target, family);

            //don't send generate/destroy events on level
            sequence.Append(new DestroyElementEvent(front.Id));
            sequence.Append(new GenerateElementEvent(virus.Id, virus.Type, true, target.Position));

            //adding virus to dictionary manualy
            elements.Add(virus);

            return sequence;
        }

        private AbstractEvent HandleUpgrade(int family)
        {
            var list = virusSpawn[family];
            var elements = list.Where(e => e.SpawnOnUpgradeVirus != Element.None).ToList();

            if (!elements.Any())
                return null;

            var i = RandomHelper.Next(0, elements.Count);
            var target = elements[i];

            var sequence = new EventsSequence();
            var cell = target.Cell;

            target.RemoveFromParent();

            var virus = level.GenerateElement(cell, target.SpawnOnUpgradeVirus);

            sequence.Append(new DestroyElementEvent(target.Id));
            sequence.Append(new GenerateElementEvent(virus.Id, virus.Type, true, cell.Position));

            //replace upgraded element in family list
            list.Remove(target);
            list.Add(virus);
            
            return sequence;
        }

        private AbstractEvent HandleUpgradeOrExpand(int type)
        {
            var handlersDefault = new VirusHandler[] { HandleExpand, HandleUpgrade };
            var handlersOther = new VirusHandler[] { HandleUpgrade, HandleExpand };

            var r = RandomHelper.Next(0, 2);

            return handlersDefault[r](type) ?? handlersOther[r](type); ;
        }

        public AbstractEvent Perform()
        {
            var events = new List<AbstractEvent>();

            foreach (var s in virusStrategies)
            {
                if (destroyOnLastTurn.Contains(s.Family))
                    continue;

                for (var i = 0; i < s.Turns; ++i)
                {
                    var e = s.Handler(s.Family);

                    if (e != null)
                        events.Add(e);
                }
            }

            destroyOnLastTurn.Clear();

            return new EventsList(events);
        }
    }
}
