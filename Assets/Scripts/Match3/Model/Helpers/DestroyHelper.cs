﻿using System.Collections.Generic;

using match3.Events;
using match3.Utils;

namespace match3.Model.Helpers
{
    internal sealed class DestroyHelper : AbstractHelper
    {
        private readonly List<AbstractEvent> events = new List<AbstractEvent>();

        public DestroyHelper(Level level) : base(level)
        {

        }

        private bool DestroyElement(Element elem, Cell cell)
        {
            if (elem == null || elem.UnBreakable || cell == null)
                return false;

            var e = new EventsSequence();

            events.Add(e);

            elem.RemoveFromParent();

            e.Append(new DestroyElementEvent(elem.Id));

            level.OnDestroyElement(elem);

            if (elem.SpawnOnDestroy == Element.None && !(elem.SpawnOnDestroyChance / 100.0 >= RandomHelper.NextDouble()))
                return true;

            var generate = level.GenerateElement(cell, elem.SpawnOnDestroy);

            e.Append(new GenerateElementEvent(generate.Id, generate.Type, true, cell.Position));

            level.OnGenerateElement(generate);

            return elem.DestroyElementsUnder;
        }

        public AbstractEvent DestroyElements(IEnumerable<Cell> cells)
        {
            events.Clear();

            foreach (var cell in cells)
            {
                var front = cell.GetFrontElement();

                while (DestroyElement(front, cell))
                    front = cell.GetFrontElement();
            }

            return new EventsList(events);
        }

        public AbstractEvent DestroyFrontElements(IEnumerable<Cell> cells)
        {
            events.Clear();

            foreach (var cell in cells)
            {
                var front = cell.GetFrontElement();

                DestroyElement(front, cell);
            }

            return new EventsList(events);
        }
    }
}
