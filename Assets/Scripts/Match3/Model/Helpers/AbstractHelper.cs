﻿
namespace match3.Model.Helpers
{
    internal abstract class AbstractHelper
    {
        protected readonly Level level;

        protected AbstractHelper(Level level)
        {
            this.level = level;
        }
    }
}
