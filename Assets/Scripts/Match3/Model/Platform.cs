﻿using System.Collections.Generic;
using System.Linq;

using match3.Exceptions;
using match3.GameData;
using match3.Events;
using match3.Geom;

namespace match3.Model
{
    internal sealed class Platform
    {
        private readonly PlatformData data;

        private readonly Level level;

        public Platform(PlatformData data, Level level)
        {
            this.data = data;
            this.level = level;
        }

        public AbstractEvent Perform()
        {
            if (data.Angle == 0)
                throw new Match3Exception("Platform rotation angle is 0 id : {0}", data.Id);

            var rotate = new Dictionary<Element, Point>();

            foreach (var rel in data.RelCells)
            {
                var p = data.Center + rel;
                var cell = level.GetCell(p);

                if (cell == null)
                    continue;

                var elements = cell.GetElements();

                foreach (var el in elements)
                    rotate[el] = cell.Position;

                cell.RemoveAllElements();
            }

            var step = data.Angle > 0 ? 90 : -90;

            for (var angle = data.Angle; angle != 0; angle -= step)
            {
                foreach (var kvp in rotate)
                {
                    var p = step >= 0 ? Rotate90Cw(kvp.Value) : Rotate90Ccw(kvp.Value);

                    var cell = level.GetCell(p);

                    if (cell == null || cell.Disabled)
                        break;

                    cell.AddElement(kvp.Key);
                }
            }

            return new RotatePlatformEvent(Id, Angle, rotate.Select(e => new RotateElements(e.Key.Id, e.Key.Cell.Position)));
        }

        public int Id
        {
            get { return data.Id; }
        }

        public int Angle
        {
            get { return data.Angle; }
        }

        private  Point Rotate90Cw(Point point)
        {
            var rel = point - data.Center;
            var tmp = rel.y;

            rel.y = rel.x;
            rel.x = -tmp;

            return data.Center + rel;
        }

        private  Point Rotate90Ccw(Point point)
        {
            var rel = point - data.Center;
            var tmp = rel.y;

            rel.y = -rel.x;
            rel.x = tmp;

            return data.Center + rel;
        }
    }
}

