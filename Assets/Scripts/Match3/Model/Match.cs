﻿using System.Collections.Generic;
using System.Linq;

using match3.Geom;

namespace match3.Model
{
    internal sealed class CompareElemements : Comparer<Element>
    {
        public override int Compare(Element a, Element b)
        {
            var posA = a.Cell.Position;
            var posB = b.Cell.Position;

            if (posA.y > posB.y)
                return 1;
            if (posA.y < posB.y)
                return -1;
            if (posA.x > posB.x)
                return 1;
            if (posA.x < posB.x)
                return -1;
            return 0;
        }
    }

    internal sealed class Match
    {
        private readonly List<Element> elements = new List<Element>();

        private readonly int spawnElement = Element.None;
        private readonly Point matchPoint;

        private readonly int priority;

        public Element[] Elements
        {
            get { return elements.ToArray(); }
        }

        public int SpawnElement
        {
            get { return spawnElement; }
        }

        public Point MatchPoint
        {
            get { return matchPoint; }
        }

        public int Priority
        {
            get { return priority; }
        }

        public Match()
        {
        }

        public Match(IEnumerable<Element> elements, int priority, int spawnElement, Point matchPoint)
        {
            this.elements.AddRange(elements);
            this.spawnElement = spawnElement;
            this.matchPoint = matchPoint;
            this.priority = priority;

            this.elements.Sort(new CompareElemements());
        }

        public bool IsCopyOrSubset(Match match)
        {
            var comparer = new CompareElemements();
            return match.elements.All(el => elements.BinarySearch(el, comparer) >= 0);
        }

        public override string ToString()
        {
            return elements.Aggregate(string.Empty, (current, el) => current + string.Format("({0},{1}) ", el.Cell.Position.x, el.Cell.Position.y));
        }
    }
}
