﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using match3.Exceptions;
using match3.GameData.Conditions;

namespace match3.Model.GameConditions
{
	internal static class ConditionsFactory
	{
		private static readonly Dictionary<Type, Type> table = new Dictionary<Type, Type>();

        public static AbstractCondition Create(Level level, AbstractConditionData data)
        {
			if (data == null)
				return null;

            if (!table.Any())
                Init();

            var type = data.GetType();

            if(!table.ContainsKey(type))
                throw new Match3Exception("Unknown condition data {0}", type.ToString());

			return Activator.CreateInstance(table[type], level, data) as AbstractCondition;
        }

		private static void Init()
		{
			var baseType = typeof(AbstractCondition);
			var assembly = Assembly.GetAssembly(baseType);

			var types = assembly.GetTypes().Where(t => t != baseType && baseType.IsAssignableFrom(t) && !t.IsAbstract);

			foreach (var t in types)
			{
				var field = t.GetField("data", BindingFlags.NonPublic | BindingFlags.Instance);
				if (field == null)
					throw new Match3Exception(@"ConditionsFactory: Cannot find field ""data"" in action {0}!", t.Name);

                table[field.FieldType] = t;
			}
		}
    }
}
