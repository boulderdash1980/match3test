﻿using System.Collections.Generic;
using System.Linq;

using match3.GameData.Conditions;
using match3.Events;

namespace match3.Model.GameConditions
{
    internal sealed class GameCondition
    {
        private readonly AbstractCondition loseCondition;
		private readonly List<WinCondition> winConditions = new List<WinCondition>();

		private readonly GameConditionData data;

        private GameEndStatus status;

		public GameCondition(GameConditionData data, Level level)
        {
			this.data = data;

            loseCondition = ConditionsFactory.Create(level, data.LoseCondition);

			if (loseCondition != null)
				loseCondition.Id = 0;

			for (var i = 0; i < data.WinConditions.Count; ++i)
			{
				var condition = ConditionsFactory.Create(level, data.WinConditions[i]) as WinCondition;

				if (condition == null)
					continue;

                condition.Id = i + 1;
                winConditions.Add(condition);
			}				
        }

		public AbstractEvent GetProgress()
		{
			return new EventsList(winConditions.Select(c => new WinConditionEvent(c.Id, c.GetProgress(), c.Data)).Cast<AbstractEvent>());
		}

        private bool IsRequiredWinConditionsDone()
        {
            return !winConditions.Any(w => !w.Check() && w.Required);
        }

        public AbstractEvent Check()
        {
            var events = new EventsSequence();

            events.Append(new EventsList(winConditions.Where(c => c.Check()).Select(c => new WinConditionEvent(c.Id, c.GetProgress(), c.Data)).Cast<AbstractEvent>()));

            status = IsRequiredWinConditionsDone() ? GameEndStatus.Victory : GameEndStatus.Lose;

            if (IsGameComplete())
            {
                events.Append(new GameEndEvent(status));
            }
            else
            {
                if (loseCondition != null && loseCondition.Check())
                    events.Append(new GameEndEvent(status));
            }

            return events;
        }

        public bool IsGameComplete()
        {
            return IsRequiredWinConditionsDone() && data.FinishAfterAllWins;
        }
    }
}
