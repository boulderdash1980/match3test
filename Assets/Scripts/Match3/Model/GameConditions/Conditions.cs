﻿using match3.GameData.Conditions;

namespace match3.Model.GameConditions
{
	internal abstract class AbstractCondition
	{
		protected Level level;
		public int Id { get; set; }

        protected AbstractCondition(Level level)
        {
            this.level = level;
        }

		public abstract AbstractConditionData Data { get; }

        public abstract bool Check();
		public abstract float GetProgress();
    }

	internal sealed class MoveEndsCondition : AbstractCondition
	{
		private readonly MovesEndsData data;

		public override AbstractConditionData Data
		{
			get { return data; }
		}

		public MoveEndsCondition(Level level, MovesEndsData condition) : base(level)
		{
			data = condition;
		}

		public override bool Check()
		{
			return level.Turn >= data.Moves;
		}

		public override float GetProgress()
		{
			return (float)level.Turn/data.Moves;
		}
	}

    internal abstract class WinCondition : AbstractCondition
    {
        private readonly WinConditionData data;

		protected WinCondition(Level level, WinConditionData data) : base(level)
        {
			this.data = data;
        }

        public bool Required
        {
			get { return data.Required; }
        }
    }

	internal sealed class DestroyAllElementsCondition : WinCondition
	{
		private readonly DestroyAllElementsData data;
		private readonly int initialCount;

		public int InitialCount
		{
			get { return initialCount; }
		}

		public override AbstractConditionData Data
		{
			get { return data; }
		}

		public DestroyAllElementsCondition(Level level, DestroyAllElementsData data) : base(level, data)
        {
            this.data = data;
			initialCount = level.GetElementCount(data.ElementType);
        }

        public override bool Check()
        {
            return level.GetElementCount(data.ElementType) == 0;
        }

		public override float GetProgress()
		{
			var remains = level.GetElementCount(data.ElementType);
			return initialCount == 0 ? 1.0f : 1.0f - (float)remains/initialCount;
		}

    }

    internal sealed class DestroyNElementsCondition : WinCondition
    {
        private int count;
		private readonly DestroyNElementsData data;

		public override AbstractConditionData Data
		{
			get { return data; }
		}

		public DestroyNElementsCondition(Level level, DestroyNElementsData data) : base(level, data)
        {
            level.onDestroyElementEvent += OnElementDestroyed;
            this.data = data;
        }

        private void OnElementDestroyed(Element e)
        {
            if (e.Type == data.ElementType)
                ++count;
        }

        public override bool Check()
        {
            return count >= data.Count;
        }

		public override float GetProgress()
		{
			return (float)count/data.Count;
		}
    }
}
