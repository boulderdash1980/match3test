using System.Collections.Generic;
using System.Linq;
using System;

using match3.Model.Helpers;
using match3.Exceptions;
using match3.GameData;
using match3.Events;
using match3.Utils;
using match3.Geom;
using match3.Info;

namespace match3.Model
{
    internal sealed class Level
    {
        private readonly Cell[,] field;

        private readonly int width;
        private readonly int height;

        private int turn;

        private readonly int widthInCells;
        private readonly int heightInCells;

        private readonly ProbabilitySchema probability;

        private readonly DropHelper dropHelper;
        private readonly MatchHelper matchHelper;

        private readonly BlastHelper blastHelper;
        private readonly DestroyHelper destroyHelper;
        private readonly VirusHelper virusHelper;

        private readonly ShuffleHelper shuffleHelper;

        private readonly List<ElementData> elementsPool = new List<ElementData>();
        private readonly List<Platform> platforms = new List<Platform>();

        private readonly List<AbstractHelper> helpers = new List<AbstractHelper>();

        //events sending by helpers or level when generating/destroying elements elements
        public event Action<Element> onGenerateElementEvent = delegate { };
        public event Action<Element> onDestroyElementEvent = delegate { };

        public int Width
        {
            get { return width; }
        }

        public int Height
        {
            get { return height; }
        }

        public int WidthInCells
        {
            get { return widthInCells; }
        }

        public int HeightInCells
        {
            get { return heightInCells; }
        }

        public int Turn
        {
            get { return turn; }
        }

        public Level(LevelData levelData, IEnumerable<MatchData> mathes, IEnumerable<ElementData> elements)
        {
            field = new Cell[levelData.Colls * 2 + 1, levelData.Rows * 2 + 1];

            width = field.Rank > 1 ? field.GetLength(1) : 0;
            height = field.GetLength(0);

            widthInCells = (width - 1)/2;
            heightInCells = (height - 1)/2;

            elementsPool.AddRange(elements);

            probability = levelData.Probability;   
            
            platforms.AddRange(levelData.Platforms.Select(e => new Platform(e, this)));

            virusHelper = new VirusHelper(this);

            foreach (var d in levelData.Cells)
            {
                var cell = new Cell(d, this);
                var pos = cell.Position;

                field[pos.y * 2 + 1, pos.x * 2 + 1] = cell;

                foreach (var e in d.Elements)
                {
                    var el = GenerateElement(cell, e.Element, e.Id);

                    OnGenerateElement(el);
                }                
            }


            destroyHelper = new DestroyHelper(this);
            dropHelper = new DropHelper(this);

            blastHelper = new BlastHelper(this, destroyHelper);
            matchHelper = new MatchHelper(this, mathes);

            shuffleHelper = new ShuffleHelper(this, matchHelper);
        }

        private void AddHelper<T>(params object[] args) where T : AbstractHelper
        {
            var helper = Activator.CreateInstance(typeof(T), args) as AbstractHelper;

            helpers.Add(helper);
        }

        private Element CloneElement(int type)
        {
            var data = elementsPool.Find(e => e.Type == type);

            if (data == null)
                throw new Match3Exception(string.Format("Can't find element by type ({0})", type));

            return new Element(data);
        }

        private KeyValuePair<int, int>[] GetProbability()
        {
            if (!probability.Config.Any())
                return probability.Elements.Select(e => new KeyValuePair<int, int>(e, 100)).ToArray();

            var last = probability.Config.Last();
            return last.Percentages;
        }

        private int GetRandomElement(IList<KeyValuePair<int, int>> probability)
        {
            if (Collections.IsNullOrEmpty(probability))
                throw new Match3Exception("Probability configuration empty!");

            if (probability.Count < 2)
                throw new Match3Exception("Probability configuration must have at least 2 elements");

            var r = RandomHelper.NextDouble();
            var vers = new double[probability.Count];

            var i = 0;

            foreach (var kvp in probability)
                vers[i++] = kvp.Value;

            var sum = vers.Sum();
            vers[0] /= sum;

            for (i = 1; i < vers.Length; i++)
                vers[i] = vers[i]/sum + vers[i - 1];

            vers[vers.Length - 1] = 1.0;

            for (i = 0; i < vers.Length; i++)
                if (vers[i] > r)
                    return probability[i].Key;

            return Element.None;
        }

        public Element GenerateElement(Cell cell, int type, string id)
        {
            var element = CloneElement(type);

            element.Id = id;

            cell.AddElement(element);

            return element;
        }

        public Element GenerateElement(Cell cell, int type)
        {
            return GenerateElement(cell, type, Guid.NewGuid().ToString());
        }

        public Element GenerateElement(Cell cell)
        {
            var type = GetRandomElement(GetProbability());

            return GenerateElement(cell, type);
        }

        public Cell GetCell(int x, int y)
        {
            if (x >= 0 && x < widthInCells && y >= 0 && y < heightInCells)
                return field[y*2 + 1, x*2 + 1];

            return null;
        }

        public Cell GetCell(Point p)
        {
            return GetCell(p.x, p.y);
        }
			
		public int GetElementCount(int type)
		{
		    return (from Cell cell in field where cell != null select cell.GetElementsCount(type)).Sum();
		}

        public Cell GetNeighbour(Cell cell, Point direction)
        {
            var p = cell.Position + direction;

            return GetCell(p);
        }

        public Cell[] GetNeighbours(Cell cell, params Point[] directions)
        {
            return (from dir in directions select GetNeighbour(cell, dir) into n where n != null select n).ToArray();
        }

        public Match FindMatch(Point p)
        {
            return matchHelper.FindMatch(p);
        }

        public Match[] FindAllMatches()
        {
            var matches = new List<Match>();

            for (var y = 0; y < heightInCells; ++y)
            {
                for (var x = 0; x < widthInCells; ++x)
                {
                    var match = matchHelper.FindMatch(x, y);

                    if (match == null)
                        continue;

                    var skip = matches.Any(m => m.IsCopyOrSubset(match));

                    if (!skip)
                        matches.Add(match);
                }
            }

            return matches.ToArray();
        }

        public Match[] FindAllPossibleMatches()
        {
            var matches = new List<Match>();
            var p = new Point();

            var directions = new[] { Directions.Right, Directions.Down };

            for (p.y = 0; p.y < heightInCells; ++p.y)
            {
                for (p.x = 0; p.x < widthInCells; ++p.x)
                {
                    foreach (var d in directions)
                    {
                        var swipe = CheckSwipe(p, p + d);

                        if (swipe == null)
                            continue;

                        foreach (var match in swipe)
                        {
                            var found = matches.Find(m => m.IsCopyOrSubset(match));

                            if (found != null)
                            {
                                if (!match.IsCopyOrSubset(found) || found.IsCopyOrSubset(match))
                                    continue;

                                matches.Add(match);
                                matches.Remove(found);
                            }
                            else
                            {
                                matches.Add(match);
                            }
                        }
                    }
                }
            }

            return matches.ToArray();
        }

        public Match[] CheckSwipe(Point p1, Point p2)
        {
            var cell1 = GetCell(p1);
            var cell2 = GetCell(p2);

            if (cell1 == null || cell2 == null)
                return null;

            var el1 = cell1.GetFrontElement();
            var el2 = cell2.GetFrontElement();

            if (el1 == null || !el1.Selectable || el2 == null || !el2.Selectable)
                return null;

            cell1.SwapFrontElements(cell2);

            var match1 = matchHelper.FindMatch(p1);
            var match2 = matchHelper.FindMatch(p2);

            cell1.SwapFrontElements(cell2);

            if (match1 != null && match2 != null)
                return new[] { match1, match2 };

            if (match1 != null)
                return new[] { match1 };

            return match2 != null ? new[] { match2 } : null;
        }

        public Match[] Swipe(Point p1, Point p2)
        {
            var matches = CheckSwipe(p1, p2);

            if (matches == null)
                return null;

            var cell1 = GetCell(p1);
            var cell2 = GetCell(p2);
            
            cell1.SwapFrontElements(cell2);

            return matches;
        }

        public Element[] GetBlastElements()
        {
            return (from Cell cell in field where cell != null && !cell.Disabled select cell.GetFrontElement() into front where front != null && front.ActivateOnInput select front).ToArray();
        }

        public BlastDirections[] GetBlastCells(IEnumerable<BlastData> directions) 
        {
            return blastHelper.GetBlastDirections(directions);
        }

        public AbstractEvent HandleBlast(IEnumerable<BlastData> directions)
        {
            return blastHelper.HandleBlast(directions);
        }

        public AbstractEvent GenerateField()
        {
            var events = new List<AbstractEvent>();

            foreach (var cell in field)
            {
                if(cell == null || cell.Disabled || cell.GetElement(ElementLayer.GamePlay) != null || cell.IsGenerationBlocked())
                    continue;

                do
                {
                    var e = GenerateElement(cell);

                    var match = FindMatch(cell.Position);

                    if (match != null)
                    {
                        e.RemoveFromParent();
                        continue;
                    }

                    events.Add(new GenerateElementEvent(e.Id, e.Type, true, cell.Position));

                    OnGenerateElement(e);

                    break;
                }
                while (true);
            }

            return new EventsList(events);
        }

        public AbstractEvent ShuffleField()
        {
            return shuffleHelper.Shuffle();
        }

        public AbstractEvent DestroyMatches(Match[] matches)
        {
            var cells = new List<Cell>();
            var neighbours = new HashSet<Cell>();

            var events = new List<AbstractEvent>();

            foreach (var match in matches)
                cells.AddRange(match.Elements.Select(e => e.Cell));

            foreach (var cell in cells)
            {
                var list = GetNeighbours(cell, Directions.Left, Directions.Right, Directions.Down, Directions.Up);

                foreach (var n in list)
                {
                    if(cells.Contains(n))
                        continue;

                    var front = n.GetFrontElement();

                    if(front == null || !front.DestroyOnMatchNear)
                        continue;

                    neighbours.Add(n);
                }
            }

            events.Add(destroyHelper.DestroyElements(cells));
            events.Add(destroyHelper.DestroyFrontElements(neighbours));

            return new EventsList(events);
        }

        public AbstractEvent ApplyCombinations(Match[] matches)
        {
            var events = new List<AbstractEvent>();

            foreach (var match in matches)
            {
                if (match.SpawnElement == Element.None)
                    continue;

                var cell = GetCell(match.MatchPoint);

                if (cell == null)
                    continue;

                var elem = CloneElement(match.SpawnElement);

                if (cell.GetElement(elem.Layer) != null)
                    continue;

                var generate = GenerateElement(cell, elem.Type);

                events.Add(new GenerateElementEvent(generate.Id, generate.Type, true, cell.Position));
            }

            return new EventsList(events);
        }

        public AbstractEvent DropAndGenerate()
        {
            return dropHelper.Process();
        }

        public AbstractEvent OnDropComplete()
        {
            return new EventsList(platforms.Select(p => p.Perform()));
        }

        public AbstractEvent OnEndTurn()
        {
            ++turn;

            return virusHelper.Perform();
        }

        public void OnGenerateElement(Element el)
        {
            onGenerateElementEvent(el);
        }

        public void OnDestroyElement(Element el)
        {
            onDestroyElementEvent(el);
        }
    }
}