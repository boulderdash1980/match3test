﻿using System.Linq;

using match3.GameData;
using match3.Geom;

namespace match3.Model
{
    internal sealed class Element
    {
        public static readonly int None = -1;

        private readonly ElementData data;

		private readonly bool isCanMatch;

        public Element(ElementData data)
        {
            this.data = data;

			var relations = data.Relation;
			isCanMatch = !UnBreakable && relations != null && relations.Any();

        }
        
        //element properties
        public Cell Cell
        {
            get; set;
        }

        public string Id
        {
            get; set;
        }

        public bool InMovement
        {
            get; set;
        }
        
        //common properties
        public ElementLayer Layer
        {
            get { return data.Layer; }
        }

        public int Type
        {
            get { return data.Type; }
        }

        public int ScoreBonus
        {
            get { return data.ScoreBonus; }
        }

        public bool Generated
        {
            get { return data.Generated; }
        }

        public bool ApplyVirus
        {
            get { return data.ApplyVirus; }
        }

        public bool Droppable
        {
            get { return data.Droppable; }
        }

        public bool Selectable
        {
            get { return data.Selectable; }
        }

        public bool IgnoreShuffle
        {
            get { return data.IgnoreShuffle; }
        }
        
        //blocker strategy
        public bool BlockMove
        {
            get { return data.BlockMove; }
        }

        public bool BlockCombination
        {
            get { return data.BlockCombination; }
        }

		public bool BlockGeneration
		{
			get { return data.BlockGeneration; }
		}

		public bool BlockMatch
		{
			get { return data.BlockMatch; }
		}

        public bool UnBreakable
        {
            get { return data.UnBreakable; }
        }
        //

        //destroy strategy
        public bool DestroyOnMatchNear
        {
            get { return data.DestroyOnMatchNear; }
        }

        public bool DestroyElementsUnder
        {
            get { return data.DestroyElementsUnder; }
        }

        public bool DestroyOnBlastNear
        {
            get { return data.DestroyOnBlastNear; }
        }

        public int SpawnOnDestroy
        {
            get { return data.SpawnOnDestroy; }
        }

        public int SpawnOnDestroyChance
        {
            get { return data.SpawnOnDestroyChance; }
        }
        //

        //virus strategy
        public bool Virus
        {
            get { return data.Virus; }
        }

        public int VirusFamily
        {
            get { return data.VirusFamily; }
        }

        public int SpawnOnUpgradeVirus
        {
            get { return data.SpawnOnUpgradeVirus; }
        }

        public int VirusTurns
        {
            get { return data.VirusTurns; }
        }

        public VirusStrategy VirusStrategy
        {
            get { return data.VirusStrategy; }
        }
        //

        //blast strategy
        public bool ActivateOnInput
        {
            get { return data.ActivateOnInput; }
        }

        public Point[] BlastDirections
        {
            get { return data.BlastDirections.ToArray(); }
        }

        public Point[] InputDirections
        {
            get { return data.InputDirecitons.ToArray(); }
        }

        public int BlastWidth
        {
            get { return data.BlastWidth; }
        }

        public int BlastHeight
        {
            get { return data.BlastHeight; }    
        }
        //

        public bool CanMatch()
        {
			return isCanMatch;
        }

        public bool CanMatch(Element other)
        {
			if (!isCanMatch || other == null)
                return false;

            return data.Relation.Any(r => other.Type == r);
        }

        public void RemoveFromParent()
        {
            if (Cell != null)
                Cell.RemoveElement(this);
        }
    }
}
