﻿using System;
using System.Linq;

using match3.Exceptions;
using match3.GameData;
using match3.Geom;

namespace match3.Model
{
	internal sealed class Cell
    {
		public static readonly Point NO_GRAVITY_FROM = new Point(-1,-1);

        private readonly Level level;
        private readonly CellData data;

		private Point gravityFrom = NO_GRAVITY_FROM;
		private bool isMatchBlocked;

        private readonly Element[] elements = new Element[(int)(ElementLayer.Size)];

        public Cell(CellData data, Level level)
        {
            this.level = level;
            this.data = data;
        }

        public void AddElement(Element element)
        {
            if (GetElement(element.Layer) != null)
                throw new Match3Exception("Cell {0} allready contains element at layer {1}", Position, element.Layer);

            element.Cell = this;
			elements[(int)element.Layer] = element;
			CheckMatchBlocked();
        }

        public void RemoveElement(Element element)
        {			
			if (elements[(int)element.Layer] != element)
                return;

            element.Cell = null;
			elements[(int)element.Layer] = null;
			CheckMatchBlocked();
		}

        public void RemoveAllElements()
        {
			foreach (var el in elements)
			{
				if(el != null)
					el.Cell = null;
			}

			isMatchBlocked = true;
			Array.Clear(elements, 0, elements.Length);
        }

        public int Count
        {
			get
			{
			    return elements.Count(e => e != null);
			}
        }

        public Point Position
        {
            get { return data.Point; }
        }

        public bool Disabled
        {
            get { return data.Disabled; }
        }

        public bool Emitter
        {
            get { return data.Emitter; }
        }

        public bool Empty
        {
            get { return Count == 0; }
        }

        public Point Gravity
        {
            get { return data.Gravity; }
        }

        public Point GravityFrom
        {
            get { return gravityFrom; }
            set { gravityFrom = value; }
        }

        public int GetElementsCount(int type)
        {
            return elements.Count(e => e!= null && e.Type == type);
        }

        public bool IsMoveBlocked()
        {
			return elements.Any(e => e != null && e.BlockMove);
        }
	
        public bool IsMoveElements()
        {
			return elements.Any(e => e != null && (e.BlockMove || e.InMovement));
        }

        public bool IsGenerationBlocked()
		{
			return elements.Any(e => e != null && e.BlockGeneration);	
		}

		public bool IsMatchBlocked()
		{			
			return isMatchBlocked;	
		}

		public void CheckMatchBlocked()
		{
			isMatchBlocked = elements.Any(e => e != null && e.BlockMatch);
		}

		public bool IsDropable()
		{
		    if (Disabled)
                return false;

			return elements[(int)ElementLayer.GamePlay] != null && !IsMoveBlocked();
		}

		public bool IsOccupied()
		{
			return elements[(int)ElementLayer.GamePlay] != null || IsGenerationBlocked();
		}

		public bool IsBlastBlocked()
        {
            var front = GetFrontElement();

            if (front == null)
                return false;

            return front.BlockCombination || front.UnBreakable;
        }

        public bool HasDroppableUpper()
        {
            for (var cell = level.GetCell(GravityFrom); cell != null; cell = level.GetCell(cell.GravityFrom))
            {
                if (cell.IsMoveBlocked())
                    return false;

                var el = cell.GetFrontElement();

                if (el != null && el.Layer == ElementLayer.GamePlay)
                    return true;
            }

            return false;
        }

        public Element GetFrontElement()
        {
			for (var i = elements.Length - 1; i >= 0; --i)
			{
				var el = elements[i];
				if (el != null)
					return el;
			}

			return null;
        }

        public Element GetElement(ElementLayer layer)
        {
			return elements[(int)layer];
        }

        public Cell GetNeighbour(Point direction)
        {
            return level.GetNeighbour(this, direction);
        }

        public Cell[] GetNeighbours(params Point[] directions)
        {
            return level.GetNeighbours(this, directions);
        }

        public Element[] GetElements()
        {
            return elements.ToArray();
        }

        public Element[] GetDropElements()
        {
            return elements.Where(e => e != null && e.Droppable).ToArray();
        }

        public bool SwapFrontElements(Cell other)
        {
            var front1 = GetFrontElement();
            var front2 = other.GetFrontElement();

            if (front1 == null || front2 == null)
                return false;

            front1.RemoveFromParent();
            front2.RemoveFromParent();

            AddElement(front2);
            other.AddElement(front1);

            return true;
        }

        public Cell PortalLink
        {
            get { return data.portal != null ? level.GetCell(data.portal.Point) : null; }
        }
    }
}
