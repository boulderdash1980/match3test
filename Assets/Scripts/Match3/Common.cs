﻿using System;

using match3.GameData.Conditions;
using match3.GameData;

namespace match3
{
    [Serializable]
    public enum ElementLayer
    {
        UnderGround = 0,
        Ground = 1,
        GamePlay = 2, // layer for drop and match elements 
        LockedVirus = 3,
        Special = 4,

        Size = 5
    }

    [Serializable]
    public enum VirusStrategy
    {
        Expand = 0,
        Upgrade = 1,
        ExpandAndUpgrade = 2
    }

    [Serializable]
    public enum GameEndStatus
    {
        Victory, Lose, NotEnoughtMatches
    }

    public static class DataTypes
    {
        public static readonly Type[] Serializable = { 
			typeof(ElementData), 
			typeof(CellData), 
			typeof(ProbabilityInfo), 
			typeof(ProbabilitySchema),

			//Conditions
			typeof(GameConditionData),
			typeof(DestroyAllElementsData),
			typeof(MovesEndsData),
			typeof(DestroyNElementsData)
		};
    }
}
