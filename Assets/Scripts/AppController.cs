﻿
using UnityEngine;
using UnityEngine.SceneManagement;

public sealed class AppController
{
    private static AppController instance;
    private string levelName;

    public static AppController Instance
    {
        get
        {
            if (instance != null)
                return instance;

            instance = new AppController();
            instance.Start();

            return instance;
        }
    }

    private void Start()
    {
        //initialize common game variables

        Application.targetFrameRate = 60;

        GameSettings.Load();
    }
		
	public void LoadMatch3Scene(string levelName)
	{
	    this.levelName = levelName;

		SceneManager.LoadScene("Match3");
	}

    public string LevelName
    {
        get { return levelName; }
    }
}
