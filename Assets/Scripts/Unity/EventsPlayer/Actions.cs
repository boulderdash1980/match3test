using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

using UnityEngine;

using match3.Events;
using match3.Geom;
using match3;

using DG.Tweening;
using View;

namespace EventsPlayer
{
    internal static class ActionFactory
    {
        private static readonly Dictionary<Type, Type> table = new Dictionary<Type, Type>();

        public static BaseAction Create(AbstractEvent e, LevelView level)
        {
            if(!table.Any())
                CreateTable();

            var t = e.GetType();

            if (!table.ContainsKey(t))
                return null;

            return Activator.CreateInstance(table[t], e, level) as BaseAction;
        }

        private static void CreateTable()
        {
            var baseType = typeof (BaseAction);

            var assembly = Assembly.GetAssembly(baseType);
            var types = assembly.GetTypes().Where(t => t != baseType && baseType.IsAssignableFrom(t) && !t.IsAbstract);

            foreach (var t in types)
            {               
                var field = t.GetField("e", BindingFlags.NonPublic | BindingFlags.Instance);
				Debug.AssertFormat(field != null, @"ActionFactory: Cannot find field ""ev"" in action {0}!", t.Name);

                table[field.FieldType] = t;
            }
        }
    }

    internal abstract class BaseAction
	{
		protected readonly LevelView level;
		private readonly AbstractEvent ae;

		public event Action<AbstractEvent, BaseAction> OnActionStart = delegate { };
        public event Action<AbstractEvent, BaseAction> OnActionDone = delegate { };

		protected BaseAction(AbstractEvent e, LevelView level)
	    {
			this.level = level;
			ae = e;
		}

		protected void DoneAction()
		{
		    OnActionDone(ae, this);
		}

		public virtual void Perform()
		{
			OnActionStart(ae, this);
		}
	}

    internal abstract class EventAction<T> : BaseAction where T : AbstractEvent
    {
        protected readonly T e;

        protected EventAction(T e, LevelView level) : base(e, level)
        {
            this.e = e;
        }
    }

    internal sealed class DestroyElementAction : EventAction<DestroyElementEvent>
    {
		public DestroyElementAction(DestroyElementEvent e, LevelView level) : base(e, level)
		{

		}

		public override void Perform()
		{
			base.Perform();

			var view = level.GetView(e.Target);
            
            Debug.AssertFormat(view != null, "Destroy Element: Can't find view by id {0}", e.Target);

            view.OnDestroyed += OnElementDestroyed;

            view.Destroy();
		}

        void OnElementDestroyed(ElementView view)
        {
            view.OnDestroyed -= OnElementDestroyed;

            DoneAction();
        }
    }

    internal sealed class GenerateElementAction : EventAction<GenerateElementEvent>
    {
        public GenerateElementAction(GenerateElementEvent e, LevelView level) : base(e, level)
        {
        }

        public override void Perform()
        {
			base.Perform();

            var view = ElementsLoader.Instance.LoadElementView(e.Id, e.Type);
            Debug.AssertFormat(view != null, "Initial Generate Element: Can't load view id - {0}, type - {1}", e.Id, e.Type);

            level.AddView(view, e.Cell);

            view.gameObject.SetActive(e.Active);

            DoneAction();
        }
    }

    internal sealed class DropElementsAction : EventAction<DropElementEvent>
    {
		public DropElementsAction(DropElementEvent e, LevelView level) : base(e, level)
		{
        }

		public override void Perform()
		{
			base.Perform();

            var view = level.GetView(e.Target);

            Debug.AssertFormat(view != null, "Drop Element: Can't find view by id {0}", e.Target);

		    var path = e.Path;

            //generated element
		    if (!view.gameObject.activeInHierarchy)
		    {
                var settings = GameSettings.Animations;

                //place element to upper cell
		        var cell = level.Data.GetCell(view.Cell);
		        var from = cell.Point - cell.Gravity;

		        view.transform.localPosition = level.ConvertCellToWorldPoint(from);

                //adding current cell to path and replace path for drop
                var list = new List<Point> { view.Cell };

                list.AddRange(path);

		        path = list.ToArray();

                //show generated element
                var renderer = view.GetComponent<SpriteRenderer>();
                var fade = renderer.FadeTo(0, 1f, settings.Drop * 0.5f);

                fade.SetDelay(settings.Drop + e.Start * settings.Drop);
            }

            view.OnDropComplete += OnDropComplete;
            view.Drop(e.Start, path);
        }

        private void OnDropComplete(ElementView view)
        {
            view.OnDropComplete -= OnDropComplete;

            DoneAction();
        }
	}

    internal sealed class RotatePlatformAction : EventAction<RotatePlatformEvent>
    {
        private readonly List<ElementView> elements = new List<ElementView>();

        public RotatePlatformAction(RotatePlatformEvent e, LevelView level) : base(e, level)
        {

        }

        public override void Perform()
        {
			base.Perform();

            var platform = level.GetPlatform(e.Platform);

            Debug.AssertFormat(platform != null, "Rotate Platform : Can't find platform by id {0}", e.Platform);

            var rotate = platform.Rotate(e.Angle);

            foreach(var el in e.Elements)
            {
                var view = level.GetView(el.element);

                Debug.AssertFormat(view != null, "Rotate Platform: Can't find view by id {0}", el.element);

                var transform = view.transform;

                view.Cell = el.to;
                transform.SetParent(platform.transform);

                elements.Add(view);
            }

            rotate.OnComplete(OnRotated);
        }

        private void OnRotated()
        {
            foreach (var view in elements)
            {
                view.transform.SetParent(level.transform);
                view.transform.localPosition = level.ConvertCellToWorldPoint(view.Cell);
            }

            DoneAction();
        }
    }

    internal sealed class SwipeFailedAction : EventAction<SwipeFailedEvent>
    {
        public SwipeFailedAction(SwipeFailedEvent e, LevelView level) : base(e, level)
        {
        }

        public override void Perform()
        {
			base.Perform();

            var a = level.GetView(e.From);
            var b = level.GetView(e.To);

            Debug.AssertFormat(a != null && b != null, "SwipeFailedAction: Can't find element at cell {0} or {1}", e.From, e.To);

            var swipe = a.Swipe(e.To);

            b.Swipe(e.From);

            swipe.OnComplete(DoneAction);
        }
    }

    internal sealed class ShowDirectionsAction : EventAction<ShowDirectionsEvent>
    {
        public ShowDirectionsAction(ShowDirectionsEvent e, LevelView level) : base(e, level)
        {
        }

        public override void Perform()
        {
			base.Perform();

            var directions = e.Drections;

            level.ClearBlastDirections();

            foreach (var dir in directions)
            {
                var view = level.GetView(dir.Id);

                Debug.AssertFormat(view != null, "Show Directions: Can't find view by id {0}", dir.Id);

                level.ShowBlastDirections(view, dir.Cells);
            }

            DoneAction();
        }
    }

	internal sealed class ShuffleFieldAction : EventAction<ShuffleFieldEvent>
    { 
		public ShuffleFieldAction(ShuffleFieldEvent e, LevelView level) : base(e, level)
		{
		}

		public override void Perform()
		{		
			var settings = GameSettings.Animations;
		    var action = DOTween.Sequence();

            base.Perform();
            
			foreach (var kvp in e.Shuffle)
			{				
				var view = level.GetView(kvp.Key);

                Debug.AssertFormat(view != null, "ShuffleFieldAction: Can't find view by id {0}", kvp.Key);

			    var cell = kvp.Value;

			    var sequence = DOTween.Sequence();
                var center = new Point(level.Width / 2, level.Height / 2);
			    var t = settings.ShuffleTime / 2.0f;

			    sequence.Append(view.transform.DOLocalMove(level.ConvertCellToWorldPoint(center), t).SetEase(Ease.OutBack));
                sequence.Append(view.transform.DOLocalMove(level.ConvertCellToWorldPoint(cell), t).SetEase(Ease.OutBack));

				view.Cell = cell;

			    action.Insert(0, sequence);
			}

		    action.OnComplete(DoneAction);
		    action.Play();
		}
	}

	internal sealed class WinConditionAction : EventAction<WinConditionEvent>
	{
		public WinConditionAction(WinConditionEvent e, LevelView level) : base(e, level)
		{

		}

		public override void Perform()
		{
			base.Perform();

			DoneAction();
		}
	}
    
	internal sealed class GameEndAction : EventAction<GameEndEvent>
	{
	    private static readonly Dictionary<GameEndStatus, string> table = new Dictionary<GameEndStatus, string>();

        public GameEndAction(GameEndEvent e, LevelView level) : base(e, level)
        {
            if (table.Any())
                return;

            table[GameEndStatus.Victory] = "UI/WinWindow";
            table[GameEndStatus.Lose] = "UI/LossWindow";
            table[GameEndStatus.NotEnoughtMatches] = "UI/LossWindow";
        }

        public override void Perform()
        {
			base.Perform();

            var scene = level.Scene;

            if(scene != null)
			    scene.ShowWindow(table[e.Status]);                  
   
			DoneAction();
        }
    }

    internal sealed class ActionList : EventAction<EventsList>
    {
        private readonly List<BaseAction> actions = new List<BaseAction>();

        public ActionList(EventsList e, LevelView level) : base(e, level)
        {
        }

        public override void Perform()
        {
			base.Perform();

            foreach (var ev in e.Events)
            {
                var action = ActionFactory.Create(ev, level);

                action.OnActionDone += OnDoneAction;

                actions.Add(action);
            }

            if (actions.Any())
            {
                var list = actions.ToList();

                list.ForEach(e => e.Perform());
            }
            else
            {
                DoneAction();
            };
        }

        private void OnDoneAction(AbstractEvent e, BaseAction a)
        {
            actions.Remove(a);

            if(!actions.Any())
                DoneAction();
        }
    }

    internal sealed class ActionSequence : EventAction<EventsSequence>
    {
        private readonly Queue<BaseAction> events = new Queue<BaseAction>();
        
        public ActionSequence(EventsSequence e, LevelView level) : base(e, level)
        {
        }

        public override void Perform()
        {
			base.Perform();

            foreach (var ev in e.Events)
            {
                var action = ActionFactory.Create(ev, level);

				action.OnActionDone += (ev1, a) => NextAction();

                events.Enqueue(action);
            }

            NextAction();
        }

        private void NextAction()
        {
            if (events.Any())
            {
                var action = events.Dequeue();

                action.Perform();
            }
            else
            {
                DoneAction();
            }
        }
    }
}