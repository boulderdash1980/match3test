﻿using System.Collections.Generic;
using System;
using System.Linq;

using UnityEngine;

using match3.Events;

using View;

namespace EventsPlayer
{
    public sealed class Player
    {
        private readonly LevelView level;

        private readonly Queue<BaseAction> actions = new Queue<BaseAction>();
        private BaseAction action;

        public event Action<Player> OnEventsPlayStart = delegate { };
        public event Action<Player> OnEventsPlayDone = delegate { };

		public event Action<AbstractEvent> OnEventStart = delegate { };
		public event Action<AbstractEvent> OnEventDone = delegate { };

        private bool playEvents;

		public Player(LevelView level)
        {
            this.level = level;
        }

        public void Update()
        {
            if (!actions.Any() || action != null)
                return;

            action = actions.Dequeue();
            action.OnActionDone += OnActionDone;
			action.OnActionStart += OnActionStart;

            if (!playEvents)
            {
                playEvents = true;

                OnEventsPlayStart(this);
            }

            action.Perform();
        }

		private void OnActionStart(AbstractEvent e, BaseAction a)
		{
			OnEventStart(e);
		}

        private void OnActionDone(AbstractEvent e, BaseAction a)
        {
            action = null;
			OnEventDone(e);

            playEvents = actions.Any();

            if (!playEvents)
                OnEventsPlayDone(this);
        }

        public void HandleEvent(AbstractEvent e)
        {
            var action = ActionFactory.Create(e, level);
            Debug.Assert(action != null, string.Format("Fabric has't create action for event {0}", e.GetType().Name));

            actions.Enqueue(action);
        }
    }
}
