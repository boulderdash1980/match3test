﻿using UI;
using UnityEngine;

namespace View
{
    public abstract class BaseSceneView : MonoBehaviour
    {
        [SerializeField]
        private UIManager guiManager;

        public void ShowWindow(string path)
        {
            if(guiManager == null)
                return;

            guiManager.ShowWindow(path);
        }
    }
}
