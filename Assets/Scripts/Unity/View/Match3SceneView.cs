﻿using System;

using UnityEngine;

namespace View
{
    internal sealed class Match3SceneView : BaseSceneView
    {
        private const string DEFAULT_LEVEL_NAME = "Levels/default";

        [SerializeField]
        private LevelController level;

        private void Start()
        {
            try
            {
                var app = AppController.Instance;

                Debug.AssertFormat(level != null, "Level is null");

				var levelName = app.LevelName;

                levelName = string.IsNullOrEmpty(levelName) ? DEFAULT_LEVEL_NAME : levelName;

                level.LoadLevel(levelName);
            }
            catch (Exception e)
            {
                Debug.LogException(e);

                Destroy(gameObject);
            }
        }
    }
}
