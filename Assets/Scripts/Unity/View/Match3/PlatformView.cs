﻿using System.Linq;

using UnityEngine;

using DG.Tweening;

using match3.Geom;
using match3.GameData;

namespace View
{
    public sealed class PlatformView : MonoBehaviour
    {
        [SerializeField]
        private float rotateDuration = 0f;

        [SerializeField]
        private Ease rotateEasing = Ease.Linear;

        public int Id
        {
            get; set;
        }

        public LevelView Level
        {
            set; get;
        }

        public float RotateDuration
        {
            get { return rotateDuration; }
        }

        public Ease RotateEasing
        {
            get { return rotateEasing; }
        }

        private struct Mask
        {
            public readonly Point Direction;
            public readonly int mask;

            public Mask(Point d, int m)
            {
                Direction = d;
                mask = m;
            }
        }

        private static readonly Mask[] Masks =
        {
        new Mask(new Point(0, -1), 1),
        new Mask(new Point(1, 0), 2),
        new Mask(new Point(0, 1), 4),
        new Mask(new Point(-1, 0), 8),
    };

        public void Load(PlatformData data)
        {
            Debug.AssertFormat(data != null, "PlatformData is null!");

            var spriteTable = GameSettings.Platforms;

            foreach (var cell in data.RelCells)
            {
                var index = (from m in Masks
                             let point = cell + m.Direction
                             where !data.RelCells.Any(p => p == point)
                             select m.mask).Sum();

                Debug.Assert(spriteTable != null, "spriteTable != null");

                if (index < 0 || index >= spriteTable.Entries.Length)
                    return;

                var info = spriteTable.Entries[index];

                var sprite = Instantiate(info.Sprite);
                var t = sprite.transform;

                t.SetParent(transform);
                t.localEulerAngles = new Vector3(0, 0, -info.RotationAngle);
                t.localPosition = new Vector2(cell.x, -cell.y) * Level.CellSize;
            }
        }

        public Tween Rotate(int angle)
        {
            var a = transform.rotation.eulerAngles + new Vector3(0, 0, -angle);
            var tween = transform.DORotate(a, rotateDuration);

            tween.SetEase(rotateEasing);

            return tween;
        }
    }
}


