﻿using System;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

using match3.GameData;
using match3.Geom;
using match3;

using DG.Tweening;

namespace View
{
    [RequireComponent(typeof(SpriteRenderer), typeof(BehaviourRGBA))]
    public sealed class ElementView : MonoBehaviour
    {
        [SerializeField]
        private ElementData data = new ElementData();

        [SerializeField]
        private Sprite blastHilightSprite;

        private Point cell = Point.Zero;

        private LevelView level;

        private bool fpsTest;

        private Animator animator;

        private Tween hintAnimation;

        public event Action<ElementView> OnDestroyed = delegate { };
        public event Action<ElementView> OnDropStart = delegate { };
        public event Action<ElementView> OnDropComplete = delegate { };
        
        //model properties
        public bool Selectable
        {
            get { return data.Selectable; }
        }

        public ElementLayer Layer
        {
            get { return data.Layer; }
        }

        public int Type
        {
            get { return data.Type; }
        }

        public bool Generated
        {
            get { return data.Generated; }
        }

        public ElementData Data
        {
            get { return data; }
        }

        //view properties
        public Point Cell
        {
            get { return cell; }
            set { cell = value; }
        }

        public LevelView Level
        {
            get { return level; }
            set { level = value; }
        }

        public string Id
        {
            get; set;
        }

        public Sprite BlastHilightSprite
        {
            get { return blastHilightSprite; }
            set { blastHilightSprite = value; }
        }

        private void Start()
        {
            var renderer = GetComponent<SpriteRenderer>();

            animator = GetComponent<Animator>();

            renderer.sortingOrder = (int)data.Layer;
        }

        private void Reset()
        {
            data = new ElementData();
        }

        public GameObject[] ShowBlastDirections(IEnumerable<Point> directions)
        {
            var loader = ElementsLoader.Instance;

            var objs = new List<GameObject>();

            if (blastHilightSprite == null)
                return objs.ToArray();

            foreach (var cell in directions)
            {
                var obj = new GameObject();

                var renderer = obj.AddComponent<SpriteRenderer>();

                renderer.sortingLayerName = "BlastHilit";
                renderer.sprite = blastHilightSprite;

                obj.transform.SetParent(level.transform);
                obj.transform.localPosition = level.ConvertCellToWorldPoint(cell);

                if (cell != this.cell)
                {
                    var arrow = Instantiate(loader.BlastArrow);

                    if (arrow != null)
                    {
                        var dir = cell - this.cell;

                        arrow.transform.rotation = Quaternion.Euler(0, 0, -dir.Angle());
                        arrow.transform.SetParent(obj.transform);
                        arrow.transform.localPosition = Vector3.zero;
                     }
                }
                else
                {
                    renderer.material.color = Color.red;
                }

                objs.Add(obj);
            }

            return objs.ToArray();
        }

        public void Destroy()
        {
            if (animator != null)
                animator.SetTrigger("destroy");
            else
                RemoveAnimationFinished();
        }

        public void RemoveAnimationFinished()
        {
            OnDestroyed(this);

            level.RemoveView(this);
        }

        public bool HandleBlast()
        {
            return data.ActivateOnInput && data.BlastDirections.Any();
        }

        public bool HandleBlast(Vector2 touch)
        {
            if (!HandleBlast())
                return false;

            var cell = level.ConvertWorldPointToCell(touch);
            return this.cell != cell;
        }

        public Point UpdateDirection(Vector2 touch)
        {
            var dir = level.ConvertWorldPointToCell(touch) - cell;

            if (dir == Point.Zero)
                return Point.Zero;

            var angle = dir.NormalizedAngle();
            var input = data.InputDirecitons.OrderBy(x => Math.Abs(x.NormalizedAngle() - angle));

            if (!input.Any())
                return Point.Zero;

            return input.First();
        }

        public void HintAnimation()
        {
            var settings = GameSettings.Animations;

            hintAnimation = transform.DOScale(new Vector3(1.2f, 1.2f, 1.2f), settings.HintShowTime)
                    .SetEase(Ease.InSine)
                    .SetLoops(2, LoopType.Yoyo);
        }

        public Tween Swipe(Point cell)
        {
            var settings = GameSettings.Animations;

            var swap = transform.DOLocalMove(level.ConvertCellToWorldPoint(cell), settings.Swap);

            swap.SetEase(Ease.OutBack);

            this.cell = cell;

            return swap;
        }

        public void OnSelect()
        {
            if (hintAnimation != null)
                hintAnimation.Kill(true);

            transform.DOScale(new Vector3(1.1f, 1.1f, 1.1f), 0.1f).SetEase(Ease.OutQuad);
        }

        public void OnDeselect()
        {
            if(hintAnimation != null)
                hintAnimation.Kill(true);

            transform.DOScale(new Vector3(1.0f, 1.0f, 1.0f), 0.1f).SetEase(Ease.OutQuad);
        }

        public void Drop(uint delay, Point[] path)
        {
            var settings = GameSettings.Animations;
            var sequence = DOTween.Sequence();

            Debug.AssertFormat(path.Length > 0, "Path is empty {0}", Id);

            var t = settings.Drop * path.Length;
            var points = path.Select(e => level.ConvertCellToWorldPoint(e).ToVector3()).ToArray();

            gameObject.SetActive(true);

            sequence.Append(transform.DOLocalPath(points, settings.Drop * points.Length).SetEase(Ease.InQuad));
            sequence.Insert(0, transform.DOScaleY(1.2f, t * 0.5f).SetLoops(2, LoopType.Yoyo));

            sequence.SetDelay(delay * settings.Drop * 1.2f);
            sequence.OnComplete(() => OnDropAnimationDone(path.Last()));

            sequence.Play();

            OnDropStart(this);
        }

        private void OnDropAnimationDone(Point cell)
        {
            var sequence = DOTween.Sequence();

            sequence.Append(transform.DOLocalMove(transform.localPosition + new Vector3(0, -2.0f, 0), 0.05f)
                .SetEase(Ease.OutSine));
			sequence.Append(transform.DOLocalMove(transform.localPosition + new Vector3(0, 3.0f, 0), 0.05f)
                .SetEase(Ease.OutSine));
			sequence.Append(transform.DOLocalMove(transform.localPosition, 0.05f)
                .SetEase(Ease.OutSine));

            this.cell = cell;

            sequence.OnComplete(() => OnDropComplete(this));
            sequence.Play();
        }

        public bool FpsTest
        {
            get { return fpsTest; }
            set
            {
                fpsTest = value;

                if (animator != null)
                    animator.SetBool("FPStest", fpsTest);
            }
        }
    }

}
