﻿using System.Collections.Generic;
using System.Collections;
using System.Linq;

using UnityEngine;

using match3.GameData;
using match3.Geom;
using match3;

namespace View
{
    public sealed class LevelView : BaseLevelView
    {
        private readonly List<ElementView> elements = new List<ElementView>();

        private readonly List<PlatformView> platforms = new List<PlatformView>();

        private readonly List<GameObject> blastHiLight = new List<GameObject>();

        private readonly List<ElementView> blastElements = new List<ElementView>();

        private readonly List<ElementView> hintElements = new List<ElementView>();

        private float hintTimer;

        [SerializeField]
        private BaseSceneView scene;

        private bool fpsTest;

        //view properties

        public ElementView[] Elements
        {
            get { return elements.ToArray(); }
        }

        public ElementView[] BlastElements
        {
            get { return blastElements.ToArray(); }
        }

        public BaseSceneView Scene
        {
            get { return scene; }
        }

        private void Start()
        {
            Refresh();
            FillBackGround();
        }

        private void Update()
        {
            if (!hintElements.Any())
                return;

            var settings = GameSettings.Animations;

            hintTimer += Time.deltaTime;

            if (hintTimer < settings.HintDelay)
                return;

            hintTimer = 0;
            hintElements.ForEach(e => e.HintAnimation());
        }

        private void OnDrawGizmos()
        {
            var x = -(Width * CellSize) / 2.0f;
            var y = -(Height * CellSize) / 2.0f;

            for (var i = 1; i < Width; ++i)
            {
                for (var j = 1; j < Height; ++j)
                {
                    Debug.DrawLine(transform.TransformPoint(new Vector2(x, y + j * CellSize)),
                                    transform.TransformPoint(new Vector2(x + Width * CellSize, y + j * CellSize)), Color.red);
                }


                Debug.DrawLine(transform.TransformPoint(new Vector2(x + i * CellSize, y)), transform.TransformPoint(new Vector2(x + i * CellSize, y + Height * CellSize)), Color.red);
            }
        }

        private void FillBackGround()
        {
            var tiles = ElementsLoader.Instance.Tiles;

            if (tiles.Count < 2 && tiles.Any(t => t == null))
                return;

            for (var i = 0; i < Height; ++i)
            {
                for (var j = 0; j < Width; ++j)
                {
                    var p = new Vector2(j, i).ToPoint();
                    var cell = data.GetCell(p);

                    if (cell.Disabled)
                        continue;

                    var tile = tiles[(j + i % 2) % 2];

                    var obj = Instantiate(tile);

                    obj.transform.parent = transform;
					obj.transform.localPosition = ConvertCellToWorldPoint(p);
                }
            }
        }

        public new void Load(LevelData data)
        {
            platforms.ForEach(e => UnityHelpers.Destroy(e.gameObject));
            elements.ForEach(e => UnityHelpers.Destroy(e.gameObject));

            platforms.Clear();
            elements.Clear();

            base.Load(data);

            var loader = ElementsLoader.Instance;

            foreach (var cell in data.Cells)
            {
                foreach (var item in cell.Elements)
                {
                    var view = loader.LoadElementView(item.Element);

                    if (view == null)
                        continue;

                    view.Id = item.Id;
                    view.Cell = cell.Point;

                    AddView(view);
                }
            }

            foreach (var platform in data.Platforms)
            {
                var view = loader.LoadPlatformView(platform);

                view.Level = this;
                view.Load(platform);

                view.transform.SetParent(transform);
                view.transform.localPosition = ConvertCellToWorldPoint(platform.Center);

                platforms.Add(view);
            }
        }

        public void AddView(ElementView view)
        {
            view.transform.SetParent(transform);
            view.Level = this;
			view.transform.localPosition = ConvertCellToWorldPoint(view.Cell);

            elements.Add(view);
        }

        public void AddView(ElementView view, Point cell)
        {
            view.Cell = cell;

            AddView(view);
        }

        public void RemoveView(ElementView view)
        {
            elements.Remove(view);

            UnityHelpers.Destroy(view.gameObject);
        }

        public ElementView GetView(Point cell, ElementLayer layer)
        {
            return elements.Find(e => e.Cell == cell && e.Layer == layer);
        }

        public ElementView GetView(Point cell)
        {
            var elements = GetElements(cell).OrderByDescending(e => e.Layer);

            return elements.Any() ? elements.First() : null;
        }

        public ElementView GetView(string id)
        {
            return elements.Find(e => e.Id == id);
        }

        public ElementView[] GetElements(Point cell)
        {
            return elements.Where(e => e.Cell == cell).ToArray();
        }

        public ElementView[] GetElements(ElementLayer layer)
        {
            return elements.Where(e => e.Layer == layer).ToArray();
        }

        public PlatformView GetPlatform(int id)
        {
            return platforms.FirstOrDefault(p => p.Id == id);
        }

        public void ShowBlastDirections(ElementView view, IEnumerable<Point> directions)
        {
            blastElements.Add(view);
            blastHiLight.AddRange(view.ShowBlastDirections(directions));
        }

        public void ClearBlastDirections()
        {
            blastHiLight.ForEach(e => Destroy(e.gameObject));
            blastHiLight.Clear();

            blastElements.Clear();
        }

        public bool FpsTest
        {
            get { return fpsTest; }
            set
            {
                fpsTest = value;

                elements.ForEach(e => e.FpsTest = value);
            }
        }

        public void ShowHintElements(IEnumerable<ElementView> elements)
        {
            hintElements.Clear();
            hintElements.AddRange(elements);

            hintTimer = 0;
        }

        public void StopHintAnimation()
        {
            hintElements.Clear();

            hintTimer = 0;
        }
    }
}
