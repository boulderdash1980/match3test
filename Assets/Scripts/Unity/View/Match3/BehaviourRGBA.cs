﻿using UnityEngine;

namespace View
{
    [RequireComponent(typeof(SpriteRenderer))]
    internal sealed class BehaviourRGBA : MonoBehaviour
    {
        private float alpha;

        private new SpriteRenderer renderer;

        private void Awake()
        {
            //initialize alpha
            renderer = GetComponent<SpriteRenderer>();

            alpha = renderer.material.color.a;
        }

        private void Start()
        {
            Update();
        }

        private void Update()
        {
            var color = renderer.material.color;

            alpha = color.a;

            var parent = transform.parent;

            if (parent != null)
            {
                var rgba = parent.GetComponent<BehaviourRGBA>();

                if (rgba != null)
                    alpha *= rgba.alpha;
            }

            color.a = alpha;

            renderer.material.color = color;
        }
    }
}
