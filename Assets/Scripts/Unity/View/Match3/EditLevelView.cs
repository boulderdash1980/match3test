﻿using System;

using match3.GameData;
using UnityEngine;

namespace View
{
    public sealed class EditLevelView : BaseLevelView
    {
        private const string Resources = "Resources/";

        [SerializeField]
        public string Path;

        [NonSerialized]
        public bool LevelLoaded;

        private void Start()
        {
            var n = Path.IndexOf(Resources, StringComparison.Ordinal);

            var path = Path.Substring(n + Resources.Length);

            n = path.IndexOf(".xml", StringComparison.Ordinal);

            path = path.Substring(0, n);

            AppController.Instance.LoadMatch3Scene(path);
        }

        public new void Load(LevelData data)
        {
            base.Load(data);

            LevelLoaded = true;
        }
    }
}

