﻿using UnityEngine;

namespace UI
{
    [RequireComponent(typeof(Canvas))]
    internal sealed class UIManager : MonoBehaviour
    {
        public void ShowWindow(string path)
        {
            var obj = Resources.Load<GameObject>(path);

            if (obj == null)
            {
                Debug.LogErrorFormat("Can't load window at path {0}", path);
                return;
            }

            var window = Instantiate(obj);
            var canvas = GetComponent<Canvas>();

            window.transform.SetParent(canvas.transform, false);
        }
    }
}


