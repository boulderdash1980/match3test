﻿using DG.Tweening;

using UnityEngine;

namespace UI
{
    [RequireComponent(typeof(CanvasGroup))]
    internal sealed class WindowBehaviour : MonoBehaviour
    {
        public static string OnWindowOpen = "OnWindowOpen";
        public static string OnWindowClose = "OnWindowClose";
        public static string OnWindowClosed = "OnWindowClosed";

        [SerializeField]
        private float showAnimationTime = 1.0f;
        
        private void Start()
        {
            var canvas = GetComponent<CanvasGroup>();

            canvas.alpha = 0;
            canvas.DOFade(1.0f, showAnimationTime);

            BroadcastMessage(OnWindowOpen);
        }

        public void Close()
        {
            var canvas = GetComponent<CanvasGroup>();

            var tween = canvas.DOFade(0, showAnimationTime);

            tween.OnComplete(OnClosed);

            BroadcastMessage(OnWindowClose);
        }

        private void OnClosed()
        {
            SendMessage(OnWindowClosed);
            Destroy(this);
        }
    }
}
