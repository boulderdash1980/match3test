﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DG.Tweening;
using UnityEngine;

namespace UI
{
    internal sealed class WindowScaler : MonoBehaviour
    {
        [SerializeField]
        private float duration = 1.0f;

        [SerializeField]
        private Ease showEasing = Ease.OutBack;

        [SerializeField]
        private Ease closeEasing = Ease.InBack;

        private void Awake()
        {
            //default scale is 0
            transform.localScale = Vector3.zero;
        }

        private void OnWindowOpen()
        {
            var tween = transform.DOScale(Vector3.one, duration);

            tween.SetEase(showEasing);
        }

        private void OnWindowClose()
        {
            var tween = transform.DOScale(Vector3.zero, duration);

            tween.SetEase(closeEasing);
        }
    }
}
