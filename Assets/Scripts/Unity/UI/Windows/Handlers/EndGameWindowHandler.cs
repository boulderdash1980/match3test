﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    internal sealed class EndGameWindowHandler : MonoBehaviour
    {
        private void OnWindowClosed()
        {
            SceneManager.LoadScene("Cats");
        }
    }
}


