﻿using UnityEngine;

using View;

namespace UI
{
    internal sealed class Match3UIHandler : MonoBehaviour
    {
        [SerializeField]
        private LevelView level;

        private void Start()
        {

        }

        public void OnFPSTestClick()
        {
            if (level != null)
            {
                level.FpsTest = !level.FpsTest;
            }
        }
    }
}
