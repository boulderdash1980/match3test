﻿using UnityEngine;

using match3.Events;

namespace UI
{
	public abstract class ConditionPanel : MonoBehaviour
	{		
		public abstract void OnCondition();
	}
}
