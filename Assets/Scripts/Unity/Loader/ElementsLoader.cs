﻿using System.Collections.Generic;
using System.Linq;
using System;

using UnityEngine;

using match3.Model;
using match3.GameData;

using View;

public sealed class ElementsLoader : MonoBehaviour
{
    private readonly List<ElementView> elements = new List<ElementView>();

    private readonly List<GameObject> tiles = new List<GameObject>();

    private GameObject blastArrow;

	public List<GameObject> Tiles
	{
		get { return tiles; }
	}

    public GameObject BlastArrow
    {
        get { return blastArrow; }
    }

    public static ElementsLoader Instance
    {
        get
        {
            var instance = FindObjectOfType<ElementsLoader>();

            if(instance == null)
            {
                var obj = new GameObject {name = "ElementsLoader"};

                instance = obj.AddComponent<ElementsLoader>();
                instance.Refresh();
            }

            instance.elements.RemoveAll(e => e == null);

            return instance;
        }
    }

    public void Refresh()
    {
        var settings = GameSettings.Assets;
        var loaded = Resources.LoadAll(settings.Elements);

        elements.Clear();
		tiles.Clear();

        var select = loaded.Select(o => o as GameObject);

        elements.AddRange(select.Select(o => o.GetComponent<ElementView>()));
        elements.RemoveAll(e => e == null || e.Type == Element.None);

		var tile1 = Resources.Load(settings.Tile1) as GameObject;
		var tile2 = Resources.Load(settings.Tile2) as GameObject;

		Debug.Assert(tile1 != null);
		Debug.Assert(tile2 != null);

		tiles.Add(tile1);
		tiles.Add(tile2);

        //temp for show directions
        blastArrow = Resources.Load("tiles/ArrowRight") as GameObject;
    }

    public ElementView LoadElementView(int type)
    {
        return LoadElementView(Guid.NewGuid().ToString(), type);
    }

    public ElementView LoadElementView(string id, int type)
    {
        var find = FindElement(type);

        if (find != null)
        {
            var clone = Instantiate(find.gameObject);
            var view = clone.GetComponent<ElementView>();

            if (view != null)
            {
                Debug.Assert(!string.IsNullOrEmpty(id), "Element id empty!");

                view.Id = id;

                return view;
            }
        }

        Debug.LogErrorFormat("Can't load view for element {0}", type);

        return null;
    }

    public PlatformView LoadPlatformView(PlatformData data)
    {
        var obj = Instantiate(Resources.Load("Active/Platform")) as GameObject;

        Debug.Assert(obj != null);

        var view = obj.GetComponent<PlatformView>();

        view.Id = data.Id;

        return view; 
    }

    public ElementView FindElement(int type)
    {
        return type == Element.None ? null : elements.Find(e => e.Type == type);
    }

    public ElementView[] Elements
    {
        get { return elements.ToArray(); }
    }

    public ElementData[] Models
    {
        get { return elements.Select(e => e.Data).ToArray(); }
    }

    public string[] TypeNames
    {
        get { return elements.Select(e => e.name).ToArray(); }
    }
}
