﻿
using UnityEngine;

public static class GameSettings
{
    private const string animations = "Settings/AnimationsSettings";
    private const string assets = "Settings/AssetsSettings";
    private const string platforms = "Settings/PlatformSpritesSettings";

    public static AnimationsSettings Animations;
    public static AssetsSettings Assets;
    public static PlatformSpritesSettings Platforms;

    //call it when application start
    public static void Load()
    {
        Animations = Resources.Load<AnimationsSettings>(animations) ?? ScriptableObject.CreateInstance<AnimationsSettings>();
        Assets = Resources.Load<AssetsSettings>(assets) ?? ScriptableObject.CreateInstance<AssetsSettings>();
        Platforms = Resources.Load<PlatformSpritesSettings>(platforms) ?? ScriptableObject.CreateInstance<PlatformSpritesSettings>();
    }
}
