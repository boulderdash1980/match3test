﻿using System.Collections.Generic;

using UnityEngine;

using match3.GameData;

public sealed class MatchDataSettings : ScriptableObject
{
    [SerializeField]
    public string Path;

    public List<MatchData> Matches = new List<MatchData>();
}

