﻿
using UnityEngine;

public sealed class AssetsSettings : ScriptableObject
{
    [SerializeField]
    public string Elements = "elements/";

    [SerializeField]
    public string Tile1 = "tiles/Tile1";

    [SerializeField]
    public string Tile2 = "tiles/Tile2";

    [SerializeField]
    public string BlastHilit = "tiles/BlastHilit";

    [SerializeField]
    public string MatchData = "GameData/MatchData";
}
