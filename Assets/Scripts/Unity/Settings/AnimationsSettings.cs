﻿using UnityEngine;

public sealed class AnimationsSettings : ScriptableObject
{
    [SerializeField]
    public float Swap = 0.1f;

    [SerializeField]
    public float Drop = 0.2f;

    [SerializeField]
    public float HintDelay = 1.0f;

    [SerializeField]
    public float HintShowTime = 0.5f;

    [SerializeField]
    public float ShuffleTime = 1.0f;
}
