﻿
internal static class Utils
{
    public static float NormalizeAngle(float a)
    {
        if (a >= 360)
        {
            a -= 360;
        }

        if (a < 0)
        {
            a += 360;
        }

        return a;
    }

    public static int NormalizeAngle(int a)
    {
        return (int)NormalizeAngle((float)a);
    }
}