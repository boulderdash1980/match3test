﻿using System;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

using DG.Tweening;

using EventsPlayer;
using View;

using match3.Events.Input;
using match3.GameData;
using match3.Geom;
using match3;

[RequireComponent(typeof(LevelView))]
internal sealed class LevelController : MonoBehaviour
{
    private const float MinTouchLenght = 5.0f;

    private LevelView level;

    private readonly GameLoop loop = new GameLoop();

    private Player player;
    private ElementView selected;
    private ElementView touched;

    private Vector3 touch;

    private bool handleInput;

    private readonly Dictionary<ElementView, Point> inputDirections = new Dictionary<ElementView, Point>();
    private readonly Dictionary<Point, Point> blastDirections = new Dictionary<Point, Point>();

    public void LoadLevel(string level)
    {
        try
        {
            var settings = GameSettings.Assets;
            var loader = ElementsLoader.Instance;

            var matches = UnityHelpers.DeserealizeFromXML<List<MatchData>>(settings.MatchData, typeof(MatchData));
            var view = GetComponent<LevelView>();

            loader.Refresh();

            var data = UnityHelpers.DeserealizeFromXML<LevelData>(level, DataTypes.Serializable);

            Debug.Assert(data != null && matches != null, "Model and GameData must be not null!");

            view.Load(data);

            player = new Player(view);

            player.OnEventsPlayStart += OnStartEvents;
            player.OnEventsPlayDone += OnEndEvents;

            handleInput = false;

            loop.onSendEvent += e => player.HandleEvent(e);
            loop.Start(data, matches, loader.Models);

            this.level = view;
        }
        catch (Exception e)
        {
            Debug.LogException(e);
            Destroy(this);
        }
    }

    private void Update()
    {
        try
        {
            loop.Update();
            player.Update();
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
    }

    private void OnMouseDown()
    {
        if (!handleInput)
            return;

        try
        {
            touch = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            var cell = level.ConvertWorldPointToCell(touch);
            var view = level.GetView(cell);

            if (view == null || !view.Selectable)
                return;

            selected = touched = view;
            selected.OnSelect();
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
    }

    private void OnMouseDrag()
    {
        if (selected == null)
            return;

        try
        {
            var location = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            var delta = location - touch;

            if (delta.magnitude < MinTouchLenght)
                return;

            touch = location;

            if (selected.HandleBlast())
                HandleBombInput();
            else
                HandleSwipe();
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
    }

    private void OnMouseUp()
    {
        try
        {
            if (selected != null)
            {
                if (touched.HandleBlast(touch))
                {
                    var directions = blastDirections.Select(e => new BlastData(e.Key, e.Value));

                    loop.HandleInput(new BlastEvent(directions));

                    handleInput = false;
                }

                selected.OnDeselect();
                selected = touched = null;
            }

            level.ClearBlastDirections();

            blastDirections.Clear();
            inputDirections.Clear();
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
    }

    private void SwapElements(ElementView a, ElementView b)
    {
        var cellA = a.Cell;
        var cellB = b.Cell;

        var renderer = a.GetComponent<SpriteRenderer>();

        renderer.sortingOrder += 1;

        var action = a.Swipe(cellB);

        b.Swipe(cellA);

        action.OnComplete(() =>
        {
            renderer.sortingOrder -= 1;

            loop.HandleInput(new SwipeEvent(cellA, cellB));
        });

        handleInput = false;
    }

    private void HandleBombInput()
    {
        var cell = level.ConvertWorldPointToCell(touch);
        var direction = cell - selected.Cell;

        var view = level.GetView(cell);

        if (view != null && view != selected)
        {
            if (level.BlastElements.Contains(view))
            {
                selected.OnDeselect();

                selected = view;
                selected.OnSelect();
            }
        }

        if (selected != touched && !selected.HandleBlast(touch))
            return;

        var updateDirection = !inputDirections.ContainsKey(selected) || inputDirections[selected] != direction;

        if (!updateDirection)
            return;

        blastDirections[selected.Cell] = selected.UpdateDirection(touch);
        inputDirections[selected] = direction;

        var directions = blastDirections.Select(e => new BlastData(e.Key, e.Value));

        loop.HandleInput(new GetDirectionsEvent(directions));
    }

    private void HandleSwipe()
    {
        var cell = level.ConvertWorldPointToCell(touch);

        var swipeHorisontal = Math.Abs(Mathf.Abs(cell.x - selected.Cell.x) - 1) < float.Epsilon &&
                              Math.Abs(cell.y - selected.Cell.y) < float.Epsilon;
        var swipeVertical = Math.Abs(Mathf.Abs(cell.y - selected.Cell.y) - 1) < float.Epsilon &&
                            Math.Abs(cell.x - selected.Cell.x) < float.Epsilon;

        if (!swipeHorisontal && !swipeVertical)
            return;

        var view = level.GetView(cell);

        if (view == null || view == selected || !view.gameObject.activeInHierarchy || !view.Selectable)
            return;

        selected.OnDeselect();

        SwapElements(view, selected);

        selected = touched = null;
    }

    private void FindHintCombination()
    {
        try
        {
            var mathes = loop.PossibleMatches.OrderByDescending(e => e.Priority).ToList();

            if (!mathes.Any())
                return;

            var match = mathes.First();
            var elements = new List<ElementView>();

            foreach (var id in match.Elements)
            {
                var view = level.GetView(id);

                Debug.AssertFormat(view != null, "Can't find element by id {0}", id);

                elements.Add(view);
            }

            level.ShowHintElements(elements);
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
    }

    private void Validate()
    {
        foreach (var view in level.Elements)
        {
            var cell = level.ConvertWorldPointToCell(view.transform.position);

            if (cell != view.Cell)
                Debug.LogErrorFormat("Cell != WP: {0} != {1}", view.Cell, cell);

            try
            {
                loop.Validate(cell, view.Id, view.Layer);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }
    }

    private void OnStartEvents(Player p)
    {
        if(!handleInput)
            level.StopHintAnimation();
    }

    private void OnEndEvents(Player p)
    {
        if(!handleInput)
        {
            handleInput = true;

            FindHintCombination();
        }

#if DEBUG || UNITY_EDITOR
        // Validate();
#endif
    }
}
