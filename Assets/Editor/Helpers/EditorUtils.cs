﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

using View;

internal static class EditorUtils
{
    public static string Popup(string label, string[] content, string val, int defaultIndex)
    {
        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.LabelField(label);

        var index = val != null ? Array.IndexOf(content, val) : -1;

        index = index == -1 ? defaultIndex : index;

        index = EditorGUILayout.Popup(index, content);

        EditorGUILayout.EndHorizontal();

        return content[index];
    }

    public static int Popup(string label, string[] content, int index)
    {
        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.LabelField(label);

        index = EditorGUILayout.Popup(index, content);

        EditorGUILayout.EndHorizontal();

        return index;
    }

    public static string GetElementTypeStr(int type, string[] types)
    {
        var find = Array.Find(types, e => e.GetHashCode() == type);

        return string.IsNullOrEmpty(find) ? EditorMain.UnknownType : find;
    }

    public static int GetElementTypeInt(string type)
    {
        return type != EditorMain.UnknownType ? type.GetHashCode() : ElementEditor.None;
    }
		
	public static Texture2D GetPreview(GameObject view)
    {
        if (view == null)
            return null;

        var renderer = view.GetComponent<SpriteRenderer>();

        return AssetPreview.GetAssetPreview(renderer.sprite);
    }

	static readonly GUILayoutOption[] PreviewSize = Rect(48,48);

	public static bool SpriteButton(GameObject view)
	{
	    return view == null ? GUILayout.Button(EditorMain.UnknownType, PreviewSize) : GUILayout.Button(GetPreview(view), PreviewSize);
	}

	public static bool ElementButton(ElementView view)
    {
		return SpriteButton(view != null ? view.gameObject : null);
	}

    public static void SelectElementButton(string label, IEnumerable<ElementView> elements, SelectElementsWindow.Event handler, int element)
    {
        EditorGUILayout.BeginVertical();

        EditorGUILayout.LabelField(label, EditorStyles.boldLabel);

        var list = elements.ToList();
        var find = list.Find(e => e.Type == element);

        if (ElementButton(find))
        {
            var window = SelectElementsWindow.Window(list);

            window.onSelectElement += handler;
            window.Show();
        }

        EditorGUILayout.EndVertical();
    }

    public static GUILayoutOption[] Rect (int width, int height)
    {
        return new [] { GUILayout.MaxWidth(width), GUILayout.MaxHeight(height), GUILayout.MinWidth(width), GUILayout.MinHeight(height) };
    }

    public static GUIContent ElementContent(ElementView view)
    {
        var loader = ElementsLoader.Instance;

		var preview = GetPreview(view.gameObject);
        var tooltip = GetElementTypeStr(view.Type, loader.TypeNames);

        return new GUIContent(preview, tooltip);
    }

    public static string FSPathToAssetPath(string path)
    {
        var n = path.IndexOf("Assets", StringComparison.Ordinal);

        return path.Substring(n);
    }
}
