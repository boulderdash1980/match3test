﻿using System.Xml.Serialization;
using System.IO;
using System.Text;

using UnityEngine;
using UnityEditor;

using match3.GameData;
using match3;

using View;

internal static class EditorMain
{
    public static readonly string DefaultCameraName = "MainCamera";
    public static readonly string DefaultLevelsFolder = "Resources/Levels/";
    public static readonly string DefaultLevelPath = "Levels/Default";

    public static readonly string UnknownType = "None";

    public static readonly Vector2 WinSize = new Vector2(1024, 768);

    [MenuItem("Editor/Setup Camera")]
    public static void SetupCamera()
    {
        var camera = Camera.main;

        if (camera == null)
        {
            var obj = new GameObject();

            obj.AddComponent<AudioListener>();

            camera = obj.AddComponent<Camera>();
            camera.tag = DefaultCameraName;
        }

        camera.name = DefaultCameraName;
        camera.orthographic = true;
        camera.orthographicSize = WinSize.y / 2;
        camera.transform.position = new Vector3(0, 0, -100);
    }

    [MenuItem("Editor/Create Level %q")]
    public static void CreateNewLevel()
    {
		RemoveLevelEditors();

        var obj = new GameObject();
        var data = UnityHelpers.DeserealizeFromXML<LevelData>(DefaultLevelPath, DataTypes.Serializable);

        var view = obj.AddComponent<EditLevelView>();

        view.Load(data);

        var path = EditorUtility.SaveFilePanel("Select XML file", DefaultLevelsFolder, "", "xml");

        if (string.IsNullOrEmpty(path))
            return;

        var formatter = new XmlSerializer(typeof(LevelData), DataTypes.Serializable);

        using (var f = new StreamWriter(path, false, Encoding.UTF8))
        {
            formatter.Serialize(f, view.Data);

            view.name = Path.GetFileNameWithoutExtension(path);
            view.Path = EditorUtils.FSPathToAssetPath(path);

            Debug.LogFormat("Create level at {0}", view.Path);
        }

        Selection.activeGameObject = view.gameObject;

        AssetDatabase.Refresh();
    }

    [MenuItem("Editor/Load Level %e")]
    public static void LoadLevel()
    {
        string[] filters = { "XML Files", "xml" };
        var path = EditorUtility.OpenFilePanelWithFilters("Open XML File", DefaultLevelsFolder, filters);

        if (string.IsNullOrEmpty(path))
            return;

		RemoveLevelEditors();
        
        var obj = new GameObject();
        var view = obj.AddComponent<EditLevelView>();

        var formatter = new XmlSerializer(typeof(LevelData), DataTypes.Serializable);

        using (var fs = new FileStream(path, FileMode.OpenOrCreate))
        {
            var data = formatter.Deserialize(fs) as LevelData;

            view.Load(data);

            view.name = Path.GetFileNameWithoutExtension(path);
            view.Path = EditorUtils.FSPathToAssetPath(path);

            Debug.LogFormat("Load level at {0}", view.Path);
        }

        Selection.activeGameObject = view.gameObject;
    }

	private static void RemoveLevelEditors()
	{
		var levels = Object.FindObjectsOfType<BaseLevelView>();

		foreach(var l in levels) Object.DestroyImmediate(l.gameObject);
	}
}
