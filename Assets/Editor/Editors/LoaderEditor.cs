﻿using System;

using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ElementsLoader))]
internal sealed class LoaderEditor : Editor
{
    private ElementsLoader Target
    {
        get { return target as ElementsLoader; }
    }

    private void OnEnable()
    {
        GameSettings.Load();

        Target.Refresh();
    }

    public override void OnInspectorGUI()
    {
        EditorGUILayout.BeginVertical();

        GUILayout.Label("Loaded Elements:", EditorStyles.boldLabel);

        var loader = ElementsLoader.Instance;
        var elements = loader.Elements;

        Array.Sort(elements, (a, b) => a.Type.CompareTo(b.Type));

        for (var j = 0; j < elements.Length; j += 6)
        {
            EditorGUILayout.BeginHorizontal();

            for (var k = j; k < elements.Length && k < j + 6; ++k)
                if (EditorUtils.ElementButton(elements[k]))
                    Selection.activeObject = elements[k].gameObject;

            EditorGUILayout.EndHorizontal();
        }

		GUILayout.Label("Tiles:", EditorStyles.boldLabel);

		EditorGUILayout.BeginHorizontal();

        foreach (var tile in loader.Tiles)
            if (EditorUtils.SpriteButton(tile))
                Selection.activeObject = tile.gameObject;

        EditorGUILayout.EndHorizontal();

        if (GUILayout.Button("Refresh", GUILayout.MaxWidth(128)))
            Target.Refresh();

        EditorGUILayout.EndVertical();
    }
}
