﻿using match3.GameData.Conditions;

using UnityEditor;

namespace LevelEditor
{
    internal abstract class ConditionEditor
    {
        protected readonly LevelEditor editor;

        protected ConditionEditor(LevelEditor editor)
        {
            this.editor = editor;
        }

        public abstract void EditCondition(AbstractConditionData condition);
    }

    internal abstract class GenericEditor<T> : ConditionEditor where T : AbstractConditionData
    {
        protected GenericEditor(LevelEditor editor) : base(editor)
        {

        }

        public override void EditCondition(AbstractConditionData condition)
        {
            var generic = condition as T;
            if (generic != null)
                EditCondition(generic);
        }

        protected abstract void EditCondition(T condition);
    }

    internal sealed class EditMovesEnd : GenericEditor<MovesEndsData>
    {
        public EditMovesEnd(LevelEditor editor) : base(editor)
        {

        }

        protected override void EditCondition(MovesEndsData condition)
        {
            condition.Moves = EditorGUILayout.IntField("Moves: ", condition.Moves);
        }
    }

    internal sealed class EditDestroyAllElements : GenericEditor<DestroyAllElementsData>
    {
        public EditDestroyAllElements(LevelEditor editor) : base(editor)
        {

        }

        protected override void EditCondition(DestroyAllElementsData condition)
        {
            condition.Required = EditorGUILayout.Toggle("Required", condition.Required);

            var loader = ElementsLoader.Instance;

            SelectElementsWindow.Event onSelectElement = w =>
            {
                condition.ElementType = w.Selected != null ? w.Selected.Type : -1;
                editor.SetUnsaved();
            };

            EditorUtils.SelectElementButton("Element: ", loader.Elements, onSelectElement, condition.ElementType);
        }
    }

    internal sealed class EditDestroyNElements : GenericEditor<DestroyNElementsData>
    {
        public EditDestroyNElements(LevelEditor editor) : base(editor)
        {

        }

        protected override void EditCondition(DestroyNElementsData condition)
        {
            var loader = ElementsLoader.Instance;

            condition.Required = EditorGUILayout.Toggle("Required", condition.Required);

            condition.Count = EditorGUILayout.IntField("Count: ", condition.Count);

            SelectElementsWindow.Event onSelectElement = w =>
            {
                condition.ElementType = w.Selected != null ? w.Selected.Type : -1;
                editor.SetUnsaved();
            };

            EditorUtils.SelectElementButton("Element: ", loader.Elements, onSelectElement, condition.ElementType);
        }
    }
}
