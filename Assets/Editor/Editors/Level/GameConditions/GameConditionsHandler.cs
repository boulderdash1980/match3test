﻿using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

using match3.GameData.Conditions;

namespace LevelEditor
{
    internal sealed class GameConditionsHandler: LevelEditorHandler
	{
	    private readonly Type[] loseConditions;
	    private readonly Type[] winConditions;

	    private readonly string[] loseConditionsStr;
	    private readonly string[] winConditionsStr;

	    private readonly Dictionary<Type, ConditionEditor> handlers = new Dictionary<Type, ConditionEditor>();

		public GameConditionsHandler(LevelEditor parent) : base(parent)
		{
		    handlers[typeof(DestroyAllElementsData)] = new EditDestroyAllElements(parent);
            handlers[typeof(DestroyNElementsData)] = new EditDestroyNElements(parent);
            handlers[typeof(MovesEndsData)] = new EditMovesEnd(parent);

            loseConditions = GetAllTypesFrom(typeof(LoseConditionData));
            winConditions = GetAllTypesFrom(typeof(WinConditionData));

		    loseConditionsStr = loseConditions.Select(e => e.Name).ToArray();
		    winConditionsStr = winConditions.Select(e => e.Name).ToArray();

            CropNames(loseConditionsStr);
            CropNames(winConditionsStr);
		}

	    private void CropNames(string[] conditions)
	    {
	        for (var i = 0; i < conditions.Length; ++i)
	        {
	            var name = conditions[i];

	            var idx = name.IndexOf("Data", StringComparison.Ordinal);

	            if (idx == -1)
                    continue;

	            name = name.Substring(0, idx);

	            conditions[i] = name;
	        }
	    }

		public override void OnInspectorGUI()
		{
			var gameCond = target.Data.GameCondition;

			EditorGUILayout.BeginVertical();

		    EditorGUILayout.BeginHorizontal();

			GUILayout.Label("Finish game if complete all win conditions");
			gameCond.FinishAfterAllWins = EditorGUILayout.Toggle(gameCond.FinishAfterAllWins);

            EditorGUILayout.EndHorizontal();

			GUILayout.Label("End game condition", EditorStyles.boldLabel);

            var index = gameCond.LoseCondition != null ?
                Array.FindIndex(loseConditions, t => t == gameCond.LoseCondition.GetType()) : 0;

		    index = EditorUtils.Popup("Select type:", loseConditionsStr, index);

			if(index != -1)
			{
				var type = loseConditions[index];

			    if (gameCond.LoseCondition == null || gameCond.LoseCondition.GetType() != type)
			        gameCond.LoseCondition = Activator.CreateInstance(type) as AbstractConditionData;

			    ShowInspectorForCondition(gameCond.LoseCondition);
			}

			EditorGUILayout.EndVertical();

			GUILayout.Label("Win conditions", EditorStyles.boldLabel);

		    var winCond = gameCond.WinConditions;

            for (var i = 0; i < winCond.Count; i++)
                EditWinCondition(i);

			if(GUILayout.Button("Add", EditorUtils.Rect(128, 24)) && winConditions.Any())
			{
			    var condition = Activator.CreateInstance(winConditions[0]) as WinConditionData;

                winCond.Add(condition);
			}

		    if (GUI.changed)
		        parent.SetUnsaved();
		}

	    private void EditWinCondition(int i)
		{			
			var conditions = target.Data.GameCondition.WinConditions;

			if(i < 0 && i >= conditions.Count)
				return;

			var cond = conditions[i];

			var index = Array.FindIndex(winConditions, t => t == cond.GetType());

            index = EditorUtils.Popup("Select type:", winConditionsStr, index);

			if(index != -1)
			{
				var type = winConditions[index];

			    if (cond.GetType() != type)
			        conditions[i] = Activator.CreateInstance(type) as WinConditionData;

			    ShowInspectorForCondition(conditions[i]);
			}

			EditorGUILayout.BeginHorizontal();
		    if (GUILayout.Button("Up", EditorUtils.Rect(128, 24)))
		        SwapConditions(i, i - 1);

		    if (GUILayout.Button("Down", EditorUtils.Rect(128, 24)))
		        SwapConditions(i, i + 1);

		    if (GUILayout.Button("Delete", EditorUtils.Rect(128, 24)))
		        conditions.Remove(cond);

		    EditorGUILayout.EndHorizontal();
		}

		private void SwapConditions(int index1, int index2)
		{			
			var conditions = target.Data.GameCondition.WinConditions;

		    if (index1 < 0 || index1 >= conditions.Count ||
		        index2 < 0 || index2 >= conditions.Count)
		        return;

		    var tmp = conditions[index1];

			conditions[index1] = conditions[index2];
			conditions[index2] = tmp;
		}

		private Type[] GetAllTypesFrom(Type type)
		{
			var assembly = Assembly.GetAssembly(type);
					return assembly.GetTypes().
				Where(t => t != type && type.IsAssignableFrom(t) && !t.IsAbstract).
						OrderBy(t => t.Name).ToArray();
		}

		private void ShowInspectorForCondition(AbstractConditionData condition)
		{
		    var type = condition.GetType();

		    if (handlers.ContainsKey(type))
		        handlers[type].EditCondition(condition);
		}
	}
}