﻿using UnityEngine;
using match3.Geom;

using View;

namespace LevelEditor
{
	internal abstract class LevelEditorHandler
	{
		protected LevelEditor parent;
		protected EditLevelView target;

	    protected LevelEditorHandler(LevelEditor parent)
		{
			this.parent = parent;
			target = parent.Target;
		}

		public abstract void OnInspectorGUI();

		public void OnSceneGUI()
		{
		}

	    public virtual void Reload()
	    {
	        
	    }

		public virtual void DrawCell(Point cell, Rect rect)
		{
		}

		public virtual void OnMouseDown(Vector2 location, Event e)
		{
		}

		public virtual void OnMouseUp(Vector2 location, Event e)
		{
		}

		public virtual void OnMouseMove(Vector2 location, Event e)
		{
		}

		public virtual void OnMouseDrag(Vector2 location, Event e)
		{
		}
	}
}