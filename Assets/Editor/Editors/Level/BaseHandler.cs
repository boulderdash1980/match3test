﻿using System;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEditor;

using match3.GameData;
using match3.Geom;

namespace LevelEditor
{
    internal sealed class BaseHandler: LevelEditorHandler
	{
		public BaseHandler(LevelEditor parent) : base(parent)
		{		
		}

		public override void OnInspectorGUI()
		{
			var data = target.Data;

			EditorGUILayout.BeginVertical();

			data.Rows = EditorGUILayout.IntField("LevelWidth", target.Width);
			data.Colls = EditorGUILayout.IntField("LevelHeight", target.Height);

			data.Cell = EditorGUILayout.IntField("Cell Size", target.CellSize);


			GUILayout.Label("Model scores", EditorStyles.boldLabel);

			EditorGUILayout.BeginHorizontal();

			GUILayout.Label("Bronze", EditorStyles.label);
			data.ScoreBronze = EditorGUILayout.IntField(data.ScoreBronze);

			GUILayout.Label("Silver", EditorStyles.label);
			data.ScoreSilver = EditorGUILayout.IntField(data.ScoreSilver);

			GUILayout.Label("Gold", EditorStyles.label);
			data.ScoreGold = EditorGUILayout.IntField(data.ScoreGold);

			EditorGUILayout.EndHorizontal();

			EditStatements();

			EditPercentages();

			GUILayout.Label("Crop level", EditorStyles.boldLabel);

			var crop = new Dictionary<string, Action> { { "Crop up", CropUp }, { "Crop down", CropDown }, { "Crop left", CropLeft }, { "Crop right", CropRight } };

		    foreach (var it in crop)
		        if (GUILayout.Button(it.Key, GUILayout.MaxWidth(200)))
		            it.Value();

		    if (GUI.changed)
		        parent.SetUnsaved();

		    EditorGUILayout.EndVertical();		
		}			

		private void EditStatements()
		{
			var probability = target.Data.Probability;

			EditorGUILayout.BeginVertical();

			GUILayout.Label("Drop match statements", EditorStyles.boldLabel);

			if (probability.Statements.Any())
			{
				var labels = new List<string> { "Moves" };

				for(var i = 0; i < (int)MatchStatement.Statement.Count; ++i)
				{
					var state = (MatchStatement.Statement)i;

					labels.Add(state.ToString());
				}

				EditorGUILayout.BeginHorizontal();

				foreach (var label in labels)
				{
				    var style = new GUIStyle { alignment = TextAnchor.MiddleCenter };

				    GUILayout.Label(label, style);
				}

				EditorGUILayout.EndHorizontal();

				foreach (var t in probability.Statements)
				{
				    EditorGUILayout.BeginHorizontal();

				    var statement = t;

				    statement.Moves = EditorGUILayout.IntField(statement.Moves);

				    const int count = (int)MatchStatement.Statement.Count;
				    var statements = new int[count];

				    if (statement.Statements.Length < count)
				        Array.Resize(ref statement.Statements, count);

				    for (var j = 0; j < count; ++j)
				        statements[j] = EditorGUILayout.IntField(statement.Statements[j]);

				    statement.Statements = statements;

				    EditorGUILayout.EndHorizontal();
				}
			}

			EditorGUILayout.BeginHorizontal();

			if (GUILayout.Button("Add statements", GUILayout.MaxWidth(128)))
			{
			    var statement = new MatchStatement {Statements = new int[(int) MatchStatement.Statement.Count]}; 

			    probability.Statements.Add(statement);
			}

		    if (probability.Statements.Any())
		        if (GUILayout.Button("Remove statements", GUILayout.MaxWidth(128)))
		            probability.Statements.Remove(probability.Statements.Last());

		    EditorGUILayout.EndHorizontal();

			EditorGUILayout.EndVertical();
		}

	    private void EditPercentages()
		{
			var loader = ElementsLoader.Instance;
			var elements = loader.Elements.Where(e => e.Generated).ToList();

			var data = target.Data;
			var probability = data.Probability;

			probability.Elements.RemoveAll(e => loader.FindElement(e) == null);

			EditorGUILayout.BeginVertical();

			GUILayout.Label("Percentage of colors", EditorStyles.boldLabel);

			EditorGUILayout.BeginHorizontal();

			GUILayout.Button("Lives", EditorUtils.Rect(48, 48));

			foreach (var view in elements)
			{
				var contains = probability.Elements.Contains(view.Type);
				var toggle = GUILayout.Toggle(contains, EditorUtils.ElementContent(view), "Button", EditorUtils.Rect(48, 48));

			    if (toggle == contains)
                    continue;

			    if (contains)
			        probability.Elements.Remove(view.Type);
			    else
			        probability.Elements.Add(view.Type);
			}

			EditorGUILayout.EndHorizontal();

			for (var i = 0; i < probability.Config.Count; ++i)
			{
				var count = 100;

				EditorGUILayout.BeginHorizontal();

				var info = probability.Config[i];

				info.Lives = EditorGUILayout.IntField(info.Lives, EditorUtils.Rect(48, 16));

				var items = new Dictionary<int, int>();

				foreach (var t in elements)
				{
				    var id = t.Type;
				    var index = Array.IndexOf(info.Percentages, Array.Find(info.Percentages, e => e.Key == id));

				    items[id] = index != -1 ? info.Percentages[index].Value : 0;
				}

				foreach(var view in elements)
				{
					var id = view.Type;
					items[id] = Math.Min(count, EditorGUILayout.IntField(items[id], EditorUtils.Rect(48, 16)));

					count -= items[id];
				}

				items.RemoveAll(e => e.Value == 0);

				info.Percentages = items.ToArray();

				probability.Config[i] = info;

				EditorGUILayout.EndHorizontal();

				EditorGUILayout.BeginHorizontal();

			    var style = new GUIStyle(EditorStyles.boldLabel) {alignment = TextAnchor.MiddleCenter};
                
			    GUILayout.Label(string.Format("Total : {0}%", count), style, GUILayout.MaxHeight(16));

				EditorGUILayout.EndHorizontal();
			}

			EditorGUILayout.BeginHorizontal();

			if (GUILayout.Button("Add scheme", GUILayout.MaxWidth(128)))
			{
				var info = new ProbabilityInfo();
				var items = elements.Where(e => probability.Elements.Contains(e.Type)).ToArray();

				info.Percentages = items.Select(e => new KeyValuePair<int, int>(e.Type, 100 / items.Length)).ToArray();

				probability.Config.Add(info);
			}

		    if (probability.Config.Any())
		        if (GUILayout.Button("Remove scheme", GUILayout.MaxWidth(128)))
		            probability.Config.Remove(probability.Config.Last());

		    EditorGUILayout.EndHorizontal();

			EditorGUILayout.EndVertical();
		}

	    private void CropUp()
		{
            RemoveRow(0);
		}

	    private void CropDown()
		{
            RemoveRow(target.Height - 1);
		}

	    private void CropLeft()
		{
            RemoveColl(0);
		}

	    private void CropRight()
		{
            RemoveColl(target.Width - 1);
		}

	    private void RemoveRow(int y)
	    {
	        if (target.Height <= 1)
                return;

	        var cells = target.Data.Cells;

	        cells.RemoveAll(e => e.Point.y == y);

	        if (y < target.Height - 1)
	            foreach (var cell in cells)
	                cell.Point -= new Point(0, 1);

	        --target.Data.Colls;

	        parent.SetUnsaved();
	        target.Refresh();
	    }

	    private void RemoveColl(int x)
	    {
	        if (target.Width <= 1)
                return;

	        var cells = target.Data.Cells;

	        cells.RemoveAll(e => e.Point.x == x);

	        if (x < target.Width - 1)
	            foreach (var cell in cells)
	                cell.Point -= new Point(1, 0);

	        --target.Data.Rows;

	        parent.SetUnsaved();
	        target.Refresh();
	    }
	}
}