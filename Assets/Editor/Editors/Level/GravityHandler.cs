﻿using System;
using System.Linq;

using UnityEngine;
using UnityEditor;

using match3.Geom;

namespace LevelEditor
{
    internal sealed class GravityHandler : LevelEditorHandler
    {
        private Point gravity = new Point(0, 1);

        private static readonly GUIContent[] buttonsContent = {
            new GUIContent((Texture) EditorGUIUtility.Load("arrow-bottom.png"), "Down"),
            new GUIContent((Texture) EditorGUIUtility.Load("arrow-top.png"), "Up"),
            new GUIContent((Texture) EditorGUIUtility.Load("arrow-left.png"), "Left"),
            new GUIContent((Texture) EditorGUIUtility.Load("arrow-right.png"), "Right"),
        };

        private static readonly Point[] buttonsToDirection = {
            new Point(0,1),
            new Point(0,-1),
            new Point(-1,0),
            new Point(1,0),
        };

        public GravityHandler(LevelEditor parent) : base(parent)
        {
        }

        public override void OnInspectorGUI()
        {
            EditorGUILayout.BeginVertical();

            var index = Array.IndexOf(buttonsToDirection, gravity);

            GUILayout.Label("Select gravity direction:", EditorStyles.boldLabel);

            index = GUILayout.Toolbar(index, buttonsContent,
                GUILayout.MaxWidth(48 * buttonsContent.Length), GUILayout.MaxHeight(48));

            gravity = buttonsToDirection[index];

            EditorGUILayout.EndVertical();

            SceneView.RepaintAll();
        }

        private struct Triangle
        {
            public readonly Point gravity;
            public readonly Vector2[] pline;

            public Triangle(Point g, Vector2[] pl)
            {
                gravity = g;
                pline = pl;
            }
        }

        private const float mult = 0.2f;
        private static readonly Triangle[] triangles = {
            new Triangle(new Point(0,-1), new[] { new Vector2(-mult,-mult), new Vector2(mult,-mult), new Vector2(0, mult), new Vector2(-mult,-mult)}),
            new Triangle(new Point(0, 1), new[] { new Vector2(-mult, mult), new Vector2(mult, mult), new Vector2(0, -mult), new Vector2(-mult, mult)}),
            new Triangle(new Point(1,0), new[] { new Vector2(-mult,-mult), new Vector2(-mult, mult), new Vector2(mult, 0), new Vector2(-mult,-mult)}),
            new Triangle(new Point(-1,0), new[] { new Vector2(mult,-mult), new Vector2(mult, mult), new Vector2(-mult, 0), new Vector2(mult,-mult)}),
        };

        public override void DrawCell(Point p, Rect rect)
        {
            var cell = target.Data.GetCell(p);
            if (cell.Disabled)
                return;

            var g = cell.Gravity;
            var tr = triangles.First(t => t.gravity == g);
            if (tr.gravity == Point.Zero)
                return;

            var pline = tr.pline.Select( v =>
            {
                Vector3 v3 = v*rect.width + rect.center;
                return v3;
            } ).ToArray();

            var color = Handles.color;

            Handles.color = Color.cyan;
            Handles.DrawPolyLine(pline);
            Handles.color = color;
        }

        public override void OnMouseDown(Vector2 location, Event e)
        {
            ChangeGravity(location);
        }

        public override void OnMouseDrag(Vector2 location, Event e)
        {
            ChangeGravity(location);
        }

        private void ChangeGravity(Vector2 location)
        {
            var p = target.ConvertWorldPointToCell(location);

            var cell = target.Data.GetCell(p);
            if (cell.Gravity == gravity)
                return;

            cell.Gravity = gravity;
            parent.SetUnsaved();
        }
    }
}