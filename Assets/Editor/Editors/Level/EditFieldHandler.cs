﻿using System;
using System.Linq;

using UnityEngine;
using UnityEditor;

using match3.GameData;
using match3;

using View;
using match3.Geom;

namespace LevelEditor
{
    internal sealed class EditFieldHandler: LevelEditorHandler
	{
		private int logicIndex = -1;

        private ElementView selected;
        private CellData portal;

	    private Action<Vector2> editAction;

		private static readonly GUIContent[] EditorButtonsContent = {
			new GUIContent((Texture) EditorGUIUtility.Load("draw.png"), "Draw elements"),
			new GUIContent((Texture) EditorGUIUtility.Load("clear.png"), "Delete elements"),
			new GUIContent((Texture) EditorGUIUtility.Load("enable.png"), "Enable cell"),
			new GUIContent((Texture) EditorGUIUtility.Load("spawn.png"), "Spawn points"),
            new GUIContent((Texture) EditorGUIUtility.Load("editorConnect.png"), "Link portals"),
        };

		private readonly Action<Vector2>[] handlers;
        
		public EditFieldHandler(LevelEditor parent) : base(parent)
		{
			handlers = new Action<Vector2>[] { OnEditElements, OnRemoveElements, OnEditCells, OnEditSpawnPoint, OnLinkPortals };
		}

		public override void OnInspectorGUI()
		{
			var visibleLayer = parent.VisibleLayer;

			EditorGUILayout.BeginVertical();

			GUILayout.Label("Field editing tools", EditorStyles.boldLabel);

			var logic = GUILayout.Toolbar(logicIndex, EditorButtonsContent,
				GUILayout.MaxWidth(48 * EditorButtonsContent.Length), GUILayout.MaxHeight(48));

			if(logic != logicIndex)
			{
				logicIndex = logic;

				selected = null;
			}

			var loader = ElementsLoader.Instance;

			EditorGUILayout.EndVertical();

		    if (logicIndex >= 0)
		        editAction = handlers[logicIndex];

		    EditorGUILayout.BeginVertical();

			EditorGUILayout.BeginHorizontal();

			GUILayout.Label("Field layers :", EditorStyles.boldLabel);

		    if (GUILayout.Button("Refresh", GUILayout.MaxWidth(128)))
		        loader.Refresh();

		    EditorGUILayout.EndHorizontal();

			for (var i = ElementLayer.UnderGround; i <= ElementLayer.Special; i++)
			{
				EditorGUILayout.LabelField("Layer " + i, EditorStyles.boldLabel);

				var visible = EditorGUILayout.Toggle("Visible", visibleLayer[i]);

			    visibleLayer[i] = visible;

				var elements = loader.Elements.Where(e => e.Layer == i).ToArray();

				for(var j = 0; j < elements.Length; j += 6)
				{
					EditorGUILayout.BeginHorizontal();

					for (var k = j; k < elements.Length && k < j + 6; ++k)
					{
						var view = elements[k];
						var toggle = GUILayout.Toggle(selected == view, EditorUtils.ElementContent(view), "Button", EditorUtils.Rect(48, 48));

					    if (!toggle)
                            continue;

					    selected = view;

					    logicIndex = 0;
					}

					EditorGUILayout.EndHorizontal();
				}
			}

			EditorGUILayout.EndVertical();
		}

        public override void DrawCell(Point cell, Rect rect)
        {
            base.DrawCell(cell, rect);

            if(portal != null && portal.Point == cell)
                Handles.DrawSolidRectangleWithOutline(rect, new Color(0f, 0.0f, 1.0f, 0.6f), Color.red);
        }

        public override void OnMouseUp(Vector2 location, Event e)
		{
		    if (editAction == null)
                return;

		    parent.SetUnsaved();

		    editAction(location);
		}

        private CellData GetCell(Vector2 location)
        {
            var pos = target.ConvertWorldPointToCell(location);

            return target.Data.GetCell(pos);
        }

	    private void OnEditCells(Vector2 location)
		{
            var cell = GetCell(location);

            if (cell == null)
                return;

            cell.Disabled = !cell.Disabled;
		}

        private void ClearPortalLink(CellData cell)
        {
            var find = parent.FindPortalLink(cell);

            if (find != null)
                find.portal = null;

            cell.portal = null;
        }

        private void OnLinkPortals(Vector2 location)
        {
            var cell = GetCell(location);

            if (cell == null)
                return;

            if (portal != null)
            {
                ClearPortalLink(cell);

                portal.portal = cell;
                portal = null;
            }
            else
            {
                ClearPortalLink(cell);

                portal = cell;
            }
        }

	    private void OnRemoveElements(Vector2 location)
		{
            var cell = GetCell(location);

		    if (cell == null)
                return;

            cell.Emitter = false;

            ClearPortalLink(cell);

            var select = cell.Elements.OrderByDescending(e => e.Layer);

            foreach (var el in select)
            {
                if (!parent.VisibleLayer[el.Layer])
                    continue;

                cell.Elements.Remove(el);

                break;
            }
        }

	    private void OnEditSpawnPoint(Vector2 location)
		{
            var cell = GetCell(location);

            if (cell == null)
                return;

            cell.Emitter = !cell.Emitter;
		}

	    private void OnEditElements(Vector2 location)
		{
		    if (selected == null)
                return;

            var cell = GetCell(location);

		    if (cell == null)
                return;

            cell.Elements.RemoveAll(e => e.Layer == selected.Layer);

            var ed = new ElementInfo
		    {
		        Element = selected.Type,
                Layer = selected.Layer,
                Id = Guid.NewGuid().ToString()
		    };

		    cell.Elements.Add(ed);
		}
	}
}