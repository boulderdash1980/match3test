﻿using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEditor;

using match3.GameData;
using match3.Geom;

namespace LevelEditor
{
	internal sealed class PlatformsHandler: LevelEditorHandler
	{
        private const float threshold = 32.0f;

        private PlatformData selectedPlatform;

		private Vector2 mouseDownPoint;
        private bool wasDragged;

		public PlatformsHandler(LevelEditor parent) : base(parent)
		{
			var platforms = target.Data.Platforms;

            selectedPlatform = platforms.FirstOrDefault();
        }

		public override void OnInspectorGUI()
		{
            var changed = false;
			EditorGUILayout.BeginVertical();

			var platforms = target.Data.Platforms;

		    selectedPlatform = selectedPlatform ?? platforms.FirstOrDefault();

            var index = -1;

            for (var i = 0; i < platforms.Count; i++)
            {
                var platform = platforms[i];
                GUILayout.Label(string.Format("Platform {0} (id={1})", i, platform.Id), EditorStyles.boldLabel);

                EditorGUILayout.BeginHorizontal();

                var angle = EditorGUILayout.IntField("Rotate", platform.Angle);
				if(angle != platform.Angle)
				{
					platform.Angle = angle;
					changed = true;
				}

				var style = new GUIStyle(GUI.skin.button);
				if(platform == selectedPlatform)
					style.normal.background = style.active.background;

                if (GUILayout.Button("Select", style))
                    selectedPlatform = platforms[i];

                if (GUILayout.Button("Delete"))
                    index = i;

                EditorGUILayout.EndHorizontal();

                GUILayout.Space(15);
            }

            if (GUILayout.Button("Add Platform"))
            {
                AddPlatform(platforms);
                changed = true; 
            }

            if (index >= 0)
            {
                changed = true;
                DeletePlatform(platforms, index);
            }

            EditorGUILayout.EndVertical();

		    if (changed)
		        parent.SetUnsaved();

		    SceneView.RepaintAll();
		}

        private void DeletePlatform(IList<PlatformData> platforms, int index)
        {
            var list = platforms.ToList();
            var deletedPlatform = platforms[index];

            list.RemoveAt(index);

            if (deletedPlatform == selectedPlatform)
                selectedPlatform = list.LastOrDefault();
          
            target.Data.Platforms = list;
        }

        private void AddPlatform(ICollection<PlatformData> platforms)
        {
            var data = new PlatformData();

            var max = platforms.Count > 0 ? platforms.Max(p => p.Id) + 1 : 1;
            var center = new Vector2(target.Width/2, target.Height/2);

            var cell = target.ConvertWorldPointToCell(center);

            data.Id = max;
            data.Center = cell;

            platforms.Add(data);
            selectedPlatform = platforms.Last();
        }

	    public override void Reload()
	    {
            var platforms = target.Data.Platforms;

            selectedPlatform = platforms.FirstOrDefault(); 
	    }

	    public override void DrawCell(Point cell, Rect rect)
		{
		    if (selectedPlatform == null)
		        return;

		    var rel = cell - selectedPlatform.Center;

		    if (selectedPlatform.RelCells.Any(c => c == rel))
		        Handles.DrawSolidRectangleWithOutline(rect, new Color(0.8f, 0.6f, 0f, 0.6f), Color.red);

		    if (rel != Point.Zero)
                return;

		    //Draw Platform center
		    var stepx = rect.width/3;
		    var stepy = rect.height/3;

		    rect.x += stepx;
		    rect.y += stepy;
		    rect.width -= stepx*2;
		    rect.height -= stepy*2;

		    Handles.DrawSolidRectangleWithOutline(rect, new Color(0f, 0f, 0f, 0.0f), Color.magenta);
		}

		public override void OnMouseDown(Vector2 location, Event e)
		{
			mouseDownPoint = location;
			wasDragged = false;
		}

		public override void OnMouseUp(Vector2 location, Event e)
		{
			if(wasDragged || Mathf.Abs(Vector2.Distance(location, mouseDownPoint)) > threshold)
				return;

			var cell = target.ConvertWorldPointToCell(location);

			var rel = cell - selectedPlatform.Center;
			var cells = selectedPlatform.RelCells;

			if(cells.Any( c => c == rel))
			{
				selectedPlatform.RelCells = cells.Where(c => c != rel ).ToArray();
			}
			else
			{
				var list = cells.ToList();
				list.Add(rel);

				selectedPlatform.RelCells = list.ToArray();
			}

			parent.SetUnsaved();
		}

		public override void OnMouseDrag(Vector2 location, Event e)
		{
			if(selectedPlatform == null || Mathf.Abs(Vector2.Distance(location, mouseDownPoint)) <= threshold)
				return;

			wasDragged = true;
			var cell = target.ConvertWorldPointToCell(location);

		    if (selectedPlatform.Center == cell)
                return;

		    selectedPlatform.Center = cell;
		    parent.SetUnsaved();
		}
	}
}