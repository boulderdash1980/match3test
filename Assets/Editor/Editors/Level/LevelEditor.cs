﻿using System.Xml.Serialization;
using System.Collections.Generic;
using System.Text;
using System.IO;

using System.Linq;
using System;

using UnityEngine;
using UnityEditor;

using match3.GameData;
using match3.Geom;
using match3;

using View;

namespace LevelEditor
{
	[CustomEditor(typeof(EditLevelView))]
	internal sealed class LevelEditor : Editor
	{
	    private readonly EditorClickDispatcher dispatcher = new EditorClickDispatcher();

		internal struct HandlerInfo
		{
			public string Name;
			public Type HandlerType;
	
			public HandlerInfo(string n, Type t)
			{
				Name = n;
				HandlerType = t;
			}
		}

		private readonly HandlerInfo[] handlers = {
			new HandlerInfo("Base", typeof(BaseHandler)),
			new HandlerInfo("Edit", typeof(EditFieldHandler)),
			new HandlerInfo("Game Conditions", typeof(GameConditionsHandler)),
			new HandlerInfo("Platforms", typeof(PlatformsHandler)),
            new HandlerInfo("Gravity", typeof(GravityHandler))
        };

	    private const string AUTOSAVE_PATH = "Assets/autosave.xml";

		private LevelEditorHandler handler;

        private int propIndex;

	    private bool unsavedChanges;

	    private readonly Dictionary<ElementLayer, bool> visibleLayer = new Dictionary<ElementLayer, bool>();

	    public EditLevelView Target
	    {
	        get { return target as EditLevelView; }
	    }
			
		public Dictionary<ElementLayer, bool> VisibleLayer
		{
			get { return visibleLayer; }
		}

		public void SetUnsaved()
		{
			unsavedChanges = true;
		    EditorUtility.SetDirty(Target);
		}

	    private void OnEnable()
        {
            for (var i = ElementLayer.UnderGround; i <= ElementLayer.Special; i++)
                visibleLayer[i] = true;

            GameSettings.Load();

            ElementsLoader.Instance.Refresh();

            if(!Target.LevelLoaded)
            {
                Load();
            }

	        dispatcher.onMouseDown += OnMouseDown;
			dispatcher.onMouseUp += OnMouseUp;
			dispatcher.onMouseMove += OnMouseMove;
			dispatcher.onMouseDrag += OnMouseDrag;
	    }

	    private void OnDisable()
	    {
	        dispatcher.onMouseDown -= OnMouseDown;

	        if (!unsavedChanges)
                return;

	        unsavedChanges = false;
	        Save(AUTOSAVE_PATH);
	    }

		private void Save(string path)
	    {
	        var serializer = new XmlSerializer(typeof(LevelData), DataTypes.Serializable);

	        using (var f = new StreamWriter(path, false, Encoding.UTF8))
	        {
	            serializer.Serialize(f, Target.Data);

	            Debug.LogFormat("Save level to {0}", path);

	            AssetDatabase.Refresh();
	        }
	    }

	    private void Load()
	    {
            var asset = AssetDatabase.LoadAssetAtPath<TextAsset>(Target.Path);

            var data = UnityHelpers.DeserealizeFromXML<LevelData>(asset, DataTypes.Serializable);

	        if (data != null)
	            Target.Load(data);

	        if (handler != null)
	            handler.Reload();
	    }
			
	    public override void OnInspectorGUI()
	    {
	        EditorGUILayout.BeginVertical();

			GUI.enabled = unsavedChanges;

            EditorGUILayout.BeginHorizontal();

	        if(GUILayout.Button("Save"))
			{
				GUI.changed = unsavedChanges = false;

				Save(Target.Path);
			}

	        if (GUILayout.Button("Revert"))
	        {
                GUI.changed = unsavedChanges = false;

                Load();
            }

            EditorGUILayout.EndHorizontal();

			GUI.enabled = true;

	        EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField(Target.Path);

	        if (GUILayout.Button("Change"))
	        {
                var path = EditorUtility.SaveFilePanel("Select XML file", EditorMain.DefaultLevelsFolder, "", "xml");

	            if (!string.IsNullOrEmpty(path))
	                Target.Path = EditorUtils.FSPathToAssetPath(path);
	        }

            EditorGUILayout.EndHorizontal();

            GUILayout.Space(10);

	        GUILayout.Label("Model properties", EditorStyles.boldLabel);

	        if (GUI.changed)
	        {
				SetUnsaved();

                Target.Refresh();
	        }

			var strings = handlers.Select(h=>h.Name).ToArray();
			propIndex = GUILayout.SelectionGrid(propIndex, strings, 3, GUILayout.MinHeight(24*((strings.Length+2) / 3)));


	        EditorGUILayout.EndVertical();

			GUILayout.Space(10);

	        if (propIndex < 0 || propIndex > handlers.Length)
	            return;

	        var handlerInfo = handlers[propIndex];
	        if (handler == null || handlerInfo.HandlerType != handler.GetType())
	            handler = Activator.CreateInstance(handlerInfo.HandlerType, new object[] {this}) as LevelEditorHandler;

	        if (handler == null)
                return;

	        GUI.changed = false;
	        handler.OnInspectorGUI();
	    }

	    private void OnSceneGUI()
	    {
	        var control = GUIUtility.GetControlID(FocusType.Passive);
	        HandleUtility.AddDefaultControl(control);

	        dispatcher.DispatchEvent(Event.current);

	        // Cells coords
	        var style = new GUIStyle();
	        var data = Target.Data;

	        style.normal.textColor = Color.yellow;
	        style.fontSize = 18;
	        style.fontStyle = FontStyle.Bold;

            var x = Target.transform.position.x - Target.Width * Target.CellSize / 2;
            var y = Target.transform.position.y - Target.Height * Target.CellSize / 2;

            var ch = 'A';

			for (var i = 0; i < Target.Width; ++i)
	        {
	            var pos = new Vector3(x + i * Target.CellSize, y);

				Handles.Label(pos + new Vector3(Target.CellSize / 2, 0), ch.ToString(), style);
				ch++;
			}

	        for (var i = 0; i < Target.Height; ++i)
	        {
	            var pos = new Vector3(x, y + i * Target.CellSize);

	            Handles.Label(pos + new Vector3(-Target.CellSize / 2, Target.CellSize), (Target.Height - i).ToString(), style);
	        }

            foreach (var pd in data.Platforms)
            {
                var backgroundColor = GUI.backgroundColor;
                GUI.backgroundColor = Color.black;

                var center = pd.Center;

                foreach (var cell in pd.RelCells)
                {
                    var pt = center + cell;
                    var position = Target.ConvertCellToWorldPoint(pt);
                    var size = Target.CellSize / 2;

                    var rect = new Rect(position.x - size, position.y - size, size * 2, size * 2);

                    Handles.DrawSolidRectangleWithOutline(rect, new Color(0, 0, 1, 0.3f), Color.black);
                }

                GUI.backgroundColor = backgroundColor;
            }

            for (var i = 0; i < Target.Width; ++i)
	        {
	            for (var j = 0; j < Target.Height; ++j)
	            {
					var p = new Point(i, j);
	                var cell = data.GetCell(p);

                    var tp = Target.transform.TransformPoint(new Vector2(x + p.x * Target.CellSize, y + (Target.Height - 1 - p.y) * Target.CellSize));
                    var rect = new Rect(tp.x, tp.y + Target.CellSize, Target.CellSize, -Target.CellSize);
                    
                    if (cell.Disabled)
                        continue;

                    var textures = new [] { "spawn.png", "holeIn.png", "holeOut.png" };
                    var conditions = new[] { cell.Emitter, cell.portal != null, FindPortalLink(cell) != null };

                    for(var k = 0; k < conditions.Length; ++k)
                    {
                        if (!conditions[k])
                            continue;

                        var texture = (Texture)EditorGUIUtility.Load(textures[k]);

                        Graphics.DrawTexture(rect, texture);
                    }

                    if(cell.portal != null)
                    {
                        var link = cell.portal.Point;
                        var ltp = Target.transform.TransformPoint(new Vector2(x + link.x * Target.CellSize, y + (Target.Height - 1 - link.y) * Target.CellSize));

                        var center = new Vector3(Target.CellSize / 2, Target.CellSize / 2);
                        var color = Handles.color;

                        Handles.color = Color.blue;

                        Handles.DrawLine(tp + center, ltp + center);
                        Handles.color = color;
                   }

                    var loader = ElementsLoader.Instance;
	                var elements = cell.Elements.Select(e => loader.FindElement(e.Element)).ToList();

                    for (var k = ElementLayer.UnderGround; k <= ElementLayer.Special; k++)
	                {
                        if(!visibleLayer[k])
                            continue;

	                    var layer = k;
	                    var select = elements.Where(e => e.Layer == layer);

	                    foreach (var el in select)
	                    {
	                        var texture = EditorUtils.GetPreview(el.gameObject);

                            if(texture == null)
                                continue;

                            Graphics.DrawTexture(rect, texture);
                        }
	                }

	                Handles.DrawSolidRectangleWithOutline(rect, new Color(0, 1, 0, 0.1f), Color.red);

	                if (handler != null)
	                    handler.DrawCell(p, rect);
	            }
	        }
	    }

        public CellData FindPortalLink(CellData data)
        {
            return Target.Data.Cells.Find(e => e.portal != null && e.portal.Point == data.Point);
        }

	    private void OnMouseDown(Event e)
	    {
			var loc = HandleEvent(e);

	        if (loc == null)
	            return;

	        handler.OnMouseDown((Vector2)loc, e);
	    }
        
	    private void OnMouseUp(Event e)
		{
			var loc = HandleEvent(e);

		    if (loc == null)
		        return;

		    handler.OnMouseUp((Vector2)loc, e);
		}

	    private void OnMouseMove(Event e)
		{
            var loc = HandleEvent(e);

		    if (loc == null)
		        return;

		    handler.OnMouseMove((Vector2)loc, e);
		}

	    private void OnMouseDrag(Event e)
		{
            var loc = HandleEvent(e);

		    if (loc == null)
		        return;

		    handler.OnMouseDrag((Vector2)loc, e);
		}

	    private Vector2? HandleEvent(Event e)
		{
		    if (Target == null || handler == null)
		        return null;

		    e.Use();

			var mouse = HandleUtility.GUIPointToWorldRay(e.mousePosition);
			var collider = Target.GetComponent<BoxCollider2D>();

			var intercept = Physics2D.GetRayIntersection(mouse);

		    if (intercept.collider != collider)
		        return null;

		    return mouse.origin;
		}
	}
}