﻿using System;
using System.Collections.Generic;
using System.Linq;

using UnityEditor;
using UnityEngine;

using match3.Geom;
using match3;

using View;

[CustomEditor(typeof(ElementView))]
internal sealed class ElementEditor : Editor
{
    public static readonly int None = -1;

    private readonly Dictionary<Point, GUIContent> directions = new Dictionary<Point, GUIContent>();

    private ElementView Target
    {
        get { return target as ElementView; }
    }

    private void OnEnable()
    {
        var loader = ElementsLoader.Instance;

        GameSettings.Load();

        loader.Refresh();

        directions[new Point(-1, -1)] = new GUIContent(EditorGUIUtility.Load("arrow-top-left.png") as Texture);
        directions[new Point(0, -1)] = new GUIContent(EditorGUIUtility.Load("arrow-top.png") as Texture);
        directions[new Point(1, -1)] = new GUIContent(EditorGUIUtility.Load("arrow-top-right.png") as Texture);
        directions[new Point(-1, 0)] = new GUIContent(EditorGUIUtility.Load("arrow-left.png") as Texture);
        directions[new Point(0, 0)] = new GUIContent(EditorGUIUtility.Load("center.png") as Texture);
        directions[new Point(1, 0)] = new GUIContent(EditorGUIUtility.Load("arrow-right.png") as Texture);
        directions[new Point(-1, 1)] = new GUIContent(EditorGUIUtility.Load("arrow-bottom-left.png") as Texture);
        directions[new Point(0, 1)] = new GUIContent(EditorGUIUtility.Load("arrow-bottom.png") as Texture);
        directions[new Point(1, 1)] = new GUIContent(EditorGUIUtility.Load("arrow-bottom-right.png") as Texture);
    }

    public override void OnInspectorGUI()
    {
        var loader = ElementsLoader.Instance;
        var types = new HashSet<string>(loader.TypeNames) {Target.name, EditorMain.UnknownType};

        EditorGUILayout.BeginVertical();

        var typeStr = EditorUtils.Popup("ElementType", types.ToArray(), EditorUtils.GetElementTypeStr(Target.Type, types.ToArray()), types.Count - 1);
        var data = Target.Data;

        GUILayout.Label("Common properties", EditorStyles.boldLabel);

        data.Type = EditorUtils.GetElementTypeInt(typeStr);

        data.Layer = (ElementLayer)EditorGUILayout.EnumPopup("Layer", Target.Layer);

        data.ScoreBonus = EditorGUILayout.IntField("Score Bonus", data.ScoreBonus);
        
        data.Generated = EditorGUILayout.Toggle("Generated", data.Generated);

        data.ApplyVirus = EditorGUILayout.Toggle("Apply Virus", data.ApplyVirus);

        data.Droppable = EditorGUILayout.Toggle("Droppable", data.Droppable);

        data.Selectable = EditorGUILayout.Toggle("Selectable", data.Selectable);

        data.IgnoreShuffle = EditorGUILayout.Toggle("Ignore Shuffle", data.IgnoreShuffle);

        EditorGUILayout.Separator();

        GUILayout.Label("Blocker properties", EditorStyles.boldLabel);

        data.BlockMove = EditorGUILayout.Toggle("Block Move", data.BlockMove);

        data.BlockCombination = EditorGUILayout.Toggle("Block Combination", data.BlockCombination);

        data.BlockGeneration = EditorGUILayout.Toggle("Block Generation", data.BlockGeneration);

        data.BlockMatch = EditorGUILayout.Toggle("Block Match", data.BlockMatch);

        data.UnBreakable = EditorGUILayout.Toggle("Unbreakable", data.UnBreakable);

        EditorGUILayout.Separator();

        GUILayout.Label("Destroy logic", EditorStyles.boldLabel);

        data.DestroyOnMatchNear = EditorGUILayout.Toggle("Break On Match Near", data.DestroyOnMatchNear);

        data.DestroyOnBlastNear = EditorGUILayout.Toggle("Break On Blast Near", data.DestroyOnBlastNear);

        data.DestroyElementsUnder = EditorGUILayout.Toggle("Destroy Elements Under", data.DestroyElementsUnder);

        EditorUtils.SelectElementButton("After destroy spawn", loader.Elements, OnSelectAfterDestroy, data.SpawnOnDestroy);

        if (data.SpawnOnDestroy != None)
        {
            GUILayout.Label("After destroy spawn chance", EditorStyles.boldLabel);

            data.SpawnOnDestroyChance = EditorGUILayout.IntSlider(data.SpawnOnDestroyChance, 0, 100);
        }
        else
        {
            data.SpawnOnDestroyChance = 0;
        }

        EditorGUILayout.Separator();

        EditRelation();

        EditorGUILayout.Separator();

        EditVirus();

        EditorGUILayout.Separator();

        EditDirections();

        EditorGUILayout.EndVertical();
        
		serializedObject.ApplyModifiedProperties();

        if (GUI.changed)
            EditorUtility.SetDirty(Target.gameObject);
    }

    private void OnSelectAfterDestroy(SelectElementsWindow w)
    {
        var data = Target.Data;

        data.SpawnOnDestroy = w.Selected != null ? w.Selected.Type : None;
        data.SpawnOnDestroyChance = 100;
    }

    private void EditDirections()
    {
        var data = Target.Data;

        GUILayout.Label("Blast logic", EditorStyles.boldLabel);

        data.ActivateOnInput = EditorGUILayout.Toggle("Activate On Input", data.ActivateOnInput);

        if(!data.ActivateOnInput)
            return;

        EditDirections("Blast Directions", data.BlastDirections, OnSelectBlastDirection);

        EditDirections("Input Directions", data.InputDirecitons, OnSelectInputDirections);

        EditorGUILayout.BeginHorizontal();

        var maxWidth = data.BlastWidth >= int.MaxValue;

        if (maxWidth)
            EditorGUILayout.LabelField("BlastWidth");
        else
            data.BlastWidth = EditorGUILayout.IntField("BlastWidth", data.BlastWidth);

        var toggle = GUILayout.Toggle(maxWidth, "Max", "Button");

        data.BlastWidth = maxWidth ? 1 : data.BlastWidth;
        data.BlastWidth = toggle ? int.MaxValue : data.BlastWidth;

        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();

        data.BlastHeight = EditorGUILayout.IntField("BlastHeight", data.BlastHeight);

        data.BlastHeight = Math.Max(1, data.BlastHeight);

        Target.BlastHilightSprite = (Sprite)EditorGUILayout.ObjectField("Blast Hilight Sprite", Target.BlastHilightSprite, typeof(Sprite), false);

        EditorGUILayout.EndHorizontal();
    }

    private void EditDirections(string label, IEnumerable<Point> dirs, SelectDirectionsWindow.Event callback)
    {
        EditorGUILayout.LabelField(label, EditorStyles.boldLabel);

        EditorGUILayout.BeginHorizontal();

        foreach (var dir in dirs)
            if (directions.ContainsKey(dir))
                EditorGUILayout.LabelField(directions[dir], EditorUtils.Rect(16, 16));

        EditorGUILayout.EndHorizontal();

        if (GUILayout.Button("Edit directions"))
        {
            var window = SelectDirectionsWindow.Window(dirs.Select(e => new Vector2(e.x, -e.y)));

            window.onWindowClosed += callback;
            window.Show();
        }
    }

    private void OnSelectInputDirections(AbstractWindow w)
    {
        var window = w as SelectDirectionsWindow;
        var data = Target.Data;

        data.InputDirecitons.Clear();
        data.InputDirecitons.AddRange(window.Selected.Select(e => new Point((int)e.x, (int)-e.y)));
    }

    private void OnSelectBlastDirection(AbstractWindow w)
    {
        var window = w as SelectDirectionsWindow;
        var data = Target.Data;

        data.BlastDirections.Clear();
        data.BlastDirections.AddRange(window.Selected.Select(e => new Point((int)e.x, (int)-e.y)));
    }

    private void EditRelation()
    {
        EditorGUILayout.BeginVertical();

        EditorGUILayout.LabelField("Relation", EditorStyles.boldLabel);

        var data = Target.Data;

        var relation = data.Relation.ToArray();
        var loader = ElementsLoader.Instance;

        loader.Refresh();

        for(var i = 0; i < relation.Length; i += 6)
        {
            EditorGUILayout.BeginHorizontal();

            for (var k = i; k < relation.Length && k < i + 6; ++k)
            {
                var id = relation[k];
                var view = loader.FindElement(id);

                if (EditorUtils.ElementButton(view))
                    data.Relation.Remove(id);
            }

            EditorGUILayout.EndHorizontal();
        }

        if (GUILayout.Button("Edit relation"))
        {
            var window = SelectElementsWindow.Window(loader.Elements);

            window.onSelectElement += OnSelectRelation;
            window.Show();
        }

        EditorGUILayout.EndVertical();
    }

    private void OnSelectRelation(SelectElementsWindow w)
    {
        if (w.Selected == null)
            return;

        var id = w.Selected.Type;

        var data = Target.Data;

        if (!data.Relation.Contains(id))
            data.Relation.Add(w.Selected.Type);
    }

    private void EditVirus()
    {
        var data = Target.Data;

        EditorGUILayout.BeginVertical();

        EditorGUILayout.LabelField("Virus Behaviour", EditorStyles.boldLabel);

        data.Virus = EditorGUILayout.Toggle("Virus", data.Virus);

        if (data.Virus)
        {
            var loader = ElementsLoader.Instance;
            var elements = loader.Elements.Where(e => e.Data.Virus).ToArray();

            EditorGUILayout.BeginHorizontal();

            EditorUtils.SelectElementButton("Virus Family", elements, OnSelectVirusFamily, data.VirusFamily);

            EditorUtils.SelectElementButton("Spawn On Upgrade Virus", elements, OnSelectUpgradeVirus, data.SpawnOnUpgradeVirus);

            EditorGUILayout.EndHorizontal();
            
            data.VirusTurns = EditorGUILayout.IntField("Virus Turns", data.VirusTurns);
            data.VirusStrategy = (VirusStrategy)EditorGUILayout.EnumPopup("Virus Strategy", data.VirusStrategy);

            foreach (var view in elements)
            {
                if(view == Target)
                    continue;

                var d = view.Data;

                if (d.VirusFamily != data.VirusFamily)
                    continue;

                d.VirusStrategy = data.VirusStrategy;
                d.VirusTurns = data.VirusTurns;

                EditorUtility.SetDirty(view.gameObject);
            }
        }

        EditorGUILayout.EndVertical();
    }

    private void OnSelectVirusFamily(SelectElementsWindow w)
    {
        var data = Target.Data;

        data.VirusFamily = w.Selected != null ? w.Selected.Type : None;
    }

    private void OnSelectUpgradeVirus(SelectElementsWindow w)
    {
        var data = Target.Data;

        data.SpawnOnUpgradeVirus = w.Selected != null ? w.Selected.Type : None;
    }
}

