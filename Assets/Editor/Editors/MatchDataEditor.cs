﻿using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
using System.Linq;

using UnityEngine;
using UnityEditor;

using match3.GameData;

[CustomEditor(typeof(MatchDataSettings))]
internal sealed class MatchDataEditor : Editor
{
    private const int Max = 5;

    private const string DefaultFolder = "Resources/GameData/";
    private const string DefaultName = "MatchData";

    private MatchDataSettings Target
    {
        get { return target as MatchDataSettings; }
    }

    private void OnEnable()
    {
        var path = Target.Path;
        var loader = ElementsLoader.Instance;

        GameSettings.Load();

        loader.Refresh();

        if(string.IsNullOrEmpty(path))
        {
            path = AssetDatabase.GetAssetPath(Target);

            path = Path.GetDirectoryName(path) + "/" + Path.GetFileNameWithoutExtension(path) + ".xml";

            Target.Path = path;
        }

        Load();
    }

    private void OnDisable()
    {
        Save();
    }
    
    public override void OnInspectorGUI()
    {
        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.LabelField(Target.Path);

        if (GUILayout.Button("Change"))
        {
            var path = EditorUtility.SaveFilePanel("Select XML file", DefaultFolder, DefaultName, "xml");

            if(!string.IsNullOrEmpty(path))
            {
                Target.Path = EditorUtils.FSPathToAssetPath(path);

                Load();
            }
        }

        EditorGUILayout.EndHorizontal();
        
        var matches = Target.Matches.ToList();

        matches.ForEach(EditMatch);

        if (GUILayout.Button("Add Match", EditorUtils.Rect(128, 24)))
            InsertNewMatch();
    }

    private void Load()
    {
        var asset = AssetDatabase.LoadAssetAtPath<TextAsset>(Target.Path);

        var matches = UnityHelpers.DeserealizeFromXML<List<MatchData>>(asset, typeof(MatchData));

        Target.Matches.Clear();

        if (matches != null)
            Target.Matches.AddRange(matches);
    }

    private void Save()
    {
        var serializer = new XmlSerializer(typeof(List<MatchData>), new[] { typeof(MatchData) });

        using (var writer = new StreamWriter(Target.Path, false, System.Text.Encoding.UTF8))
        {
            Debug.Log(string.Format("Save matchData to {0}", Target.Path));

            serializer.Serialize(writer, Target.Matches);

            Debug.Log(string.Format("Save matchData: count = {0}", Target.Matches));
            Debug.Log("Save matchData: success");
        }

        AssetDatabase.Refresh();
    }

    private void EditMatch(MatchData match)
    {
        EditorGUILayout.BeginVertical();

        var matchName = match.Name ?? string.Empty;

        EditorGUILayout.LabelField(string.Format("Match {0}", matchName), EditorStyles.boldLabel);

        match.Name = EditorGUILayout.TextField(matchName);

        SelectElementsWindow.Event onSelectElement = window =>
        {
            match.SpawnElement = window.Selected != null ? window.Selected.Type : ElementEditor.None;
        };

        const int size = 24;

        for (var y = 0; y < Max; ++y)
        {
            EditorGUILayout.BeginHorizontal();

            for (var x = 0; x < Max; ++x)
            {
                var toggle = GUILayout.Toggle(match[x, y] == 1, "", "Button", EditorUtils.Rect(size, size));

                match[x, y] = toggle ? 1 : 0;
            }

            EditorGUILayout.EndHorizontal();
        }

        var loader = ElementsLoader.Instance;

        EditorUtils.SelectElementButton("OnMatch:", loader.Elements, onSelectElement, match.SpawnElement);


		match.IgnoreIfNoMatchesFound = EditorGUILayout.Toggle("Ignore if no matches found", match.IgnoreIfNoMatchesFound);

        EditorGUILayout.BeginHorizontal();

        var matches = Target.Matches;

        if (GUILayout.Button("Delete Match", EditorUtils.Rect(128, 24)))
            matches.Remove(match);

        if (matches.Count > 1)
        {
            var index = matches.IndexOf(match);

            if (index + 1 < matches.Count && GUILayout.Button("Move Down", EditorUtils.Rect(96, 24)))
                SwapMatches(match, matches[index + 1]);

            if (index > 0 && GUILayout.Button("Move Up", EditorUtils.Rect(96, 24)))
                SwapMatches(match, matches[index - 1]);
        }

        EditorGUILayout.EndHorizontal();

        EditorGUILayout.EndVertical();
    }

    private void InsertNewMatch()
    {
        var match = new MatchData(Max, Max);
 
        Target.Matches.Add(match);
    }

    private void SwapMatches(MatchData m1, MatchData m2)
    {
        var matches = Target.Matches;

        var i1 = matches.IndexOf(m1);
        var i2 = matches.IndexOf(m2);

        matches[i2] = m1;
        matches[i1] = m2;
    }
}
