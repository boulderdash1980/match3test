﻿
using UnityEditor;

internal abstract class AbstractWindow : EditorWindow
{
    public delegate void Event(AbstractWindow w);

    public event Event onWindowClosed = delegate { };

    private void OnDestroy()
    {
        onWindowClosed(this);
    }
}